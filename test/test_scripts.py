# encoding: utf8


from biseau.core.scripts import (options_description_from_module,
                                 topological_sort_by_io)


def test_options_description_from_module():
    expected = {'arg1': 'Should be ok !',
                'arg2': '(αnd thĳs should ßə 2 !)'}  # arg3 have no match

    class module:  # mocking the run_on with doc
        def run_on():
            pass
    module.run_on.__doc__ = """Text that should not be matched -- ever

    even -- if it matches the regex

    arg1 -- {}
    arg2 -- {}
    arg3 - but not this one (needs two dashes)

    """.format(expected['arg1'], expected['arg2'])

    found = options_description_from_module(module, frozenset(expected.keys()))
    assert found == expected


def test_topological_sort_by_io__chaining():
    inputs  = {'b': {1}, 'c': {2}}
    outputs = {'a': {1}, 'b': {2}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) == 'abc'

def test_topological_sort_by_io__two_sources():
    inputs  = {'c': {1, 2}}
    outputs = {'a': {1}, 'b': {2}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) in {'abc', 'bac'}

def test_topological_sort_by_io__two_preds():
    inputs  = {'b': {1}, 'c': {1, 2}}
    outputs = {'a': {1}, 'b': {2}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) == 'abc'

def test_topological_sort_by_io__two_succs():
    inputs  = {'a': set(), 'b': {1}, 'c': {1}}
    outputs = {'a': {1}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) in {'abc', 'acb'}

def test_topological_sort_by_io__cycle():
    inputs  = {'a': {2}, 'b': {1}}
    outputs = {'a': {1}, 'b': {2}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) in {'ab', 'ba'}

def test_topological_sort_by_io__long_cycle():
    inputs  = {'a': {4}, 'b': {1}, 'c': {2}, 'd': {3}}
    outputs = {'a': {1}, 'b': {2}, 'c': {3}, 'd': {4}}
    assert ''.join(topological_sort_by_io(inputs, outputs)) in {'abcd', 'bcda', 'cdab', 'dabc'}
