# encoding: utf8

import os
from functools import partial
from biseau.utils import color_from_colors, normalized_path, ispartial


def test_colors():
    assert color_from_colors({'blue', 'red'}) == 'magenta'
    assert color_from_colors({'blue', 'green'}) == 'cyan'
    assert color_from_colors({'red', 'green'}) == 'yellow'
    assert color_from_colors({'red', 'green', 'blue'}) == 'white'
    assert color_from_colors({'transparent'}) == 'transparent'


def test_normalized_path():
    assert normalized_path('~/a') == os.path.expanduser('~/a')


def test_ispartial():
    def f(): pass
    assert ispartial(f, f)
    assert ispartial(partial(f), f)
    assert ispartial(partial(partial(f)), f)
    assert ispartial(partial(partial(partial(f))), f)

def test_ispartial_class():
    class A: pass
    class B(A): pass
    C = partial(B, opt='ion')
    _ispartial = partial(ispartial, subclass=True)

    assert _ispartial(A, A)
    assert _ispartial(partial(A), A)

    assert _ispartial(partial(partial(B)), A)
    assert _ispartial(partial(partial(B)), B)

    assert _ispartial(partial(partial(C)), A)
    assert _ispartial(partial(partial(C)), B)

def test_ispartial_instance():
    class A: pass
    class B(A): pass
    _ispartial = partial(ispartial, instance=True)
    assert _ispartial(A, type)
    assert _ispartial(B, type)
    assert _ispartial(partial(A), type)
    assert _ispartial(partial(B), type)
