# encoding: utf8
"""Widget allowing user to write its own python code
and run details.

"""

import os
import itertools
import traceback
import tkinter as tk
from tkinter import font
import clyngor
from biseau.gui import ToolTip, SimpleCheckbox, Runnable, RuleSaverWindow, UserCodeWidget


USER_TEXT_COLUMNS, USER_TEXT_LINES = 50, 6
USER_TEXT_FONTSIZE = 10
FAVORITE_FONT_FAMILIES = 'Source Code Pro', 'Source Sans Pro'
EDITOR_WELCOME_MESSAGE = r"""# Create your own rules here.

for model in models:
    for concept, intent in model.get('int', ()):
        if intent == '"adult"':
            yield 'color({},red).'.format(concept)
"""
DEFAULT_TOOLTIP = (
    "You have access to 'model', a mapping "
    "{predicate: args} containing the model.\n"
    "Just yield or return what you want to add to the context."
)

class UserPythonCodeWidget(UserCodeWidget):


    def __init__(self, master, rule_per_line:int=4,
                 welcome_message:str=EDITOR_WELCOME_MESSAGE,
                 name="Write your own {language}",
                 tooltip:str=DEFAULT_TOOLTIP):
        self.rule_per_line = int(rule_per_line)
        self.__error = '# no error'
        super().__init__(master, comment_char='#', rule_char='\n',
                         rule_per_line=4, language='python', openable=True,
                         welcome_message=welcome_message,
                         name=str(name), tooltip=str(tooltip))


    def _run_on(self, context:str):
        try:
            func = self.run_code()
            models = clyngor.solve((), inline=context, print_command=True).by_predicate.int_not_parsed.careful_parsing
            application = ''.join(func(models))
        except:
            self.__error = traceback.format_exc()
            return context
        else:
            self.__error = '%%Generated atoms:\n' + application
        return context + '\n' + application


    def run_code(self):
        code = 'def func(models):' + ''.join(
            '\n    ' + line for line in self.user_rule.splitlines(False)
        )
        namespace = {}
        exec(code, namespace)
        return namespace['func']

    def _open_sources(self) -> str:
        return self.__error
