# encoding: utf8
"""Definition of the high level object, Script.

"""
import os
import tkinter as tk
from tkinter import ttk
from collections import namedtuple
from biseau import utils
from biseau.core.scripts import Script
from biseau.gui import ToolTip, SimpleCheckbox, FileButton, CodeEditor, Runnable
from biseau.utils import normalized_path, ispartial, ispartialsubclass
import clyngor


class ScriptWidget(Runnable):
    """Widget for a Script"""

    def __init__(self, master, script:Script=None, active:bool=None):
        self.script = script or self
        state = active if active is not None else self.script.active_by_default
        super().__init__(master, name=self.script.name, state=state)


    def create_widgets(self) -> {str: tk.Widget}:
        wid_options = {}
        for idx, option in enumerate(self.script.options, start=1):
            option_name, option_type, option_value, option_desc = option
            opt_name = option_name.replace('_', ' ')
            if option_type is int:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                spn = tk.Spinbox(wid, increment=1, command=self.update)
                spn.grid(row=0, column=1, sticky=tk.W)
                spn.delete(0, 'end')
                spn.insert(0, int(option_value))
                wid.get = lambda spn=spn: spn.get()
                wid.configure = spn.configure
            elif option_type is float:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                spn = tk.Spinbox(wid)
                spn.grid(row=0, column=1, sticky=tk.W)
                spn.delete(0, 'end')
                spn.insert(0, float(option_value))
                wid.get = lambda spn=spn: float(spn.get())
                wid.configure = spn.configure
            elif option_type is bool:
                wid = SimpleCheckbox(self, text=opt_name,
                                     state=bool(option_value),
                                     on_state_change=self.update)
            elif option_type is list:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                txt = tk.StringVar(wid, str(next(iter(option_value))))
                cmb = ttk.Combobox(wid, textvariable=txt,
                                   justify='left', state='readonly',
                                   values=tuple(option_value))
                cmb.bind('<<ComboboxSelected>>', lambda _:self.update())
                cmb.grid(row=0, column=1, sticky=tk.E)
                wid.get = lambda cmb=cmb: cmb.get()
                wid.configure = cmb.configure
            elif option_type is str:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.W)
                nb_lines = len(option_value.splitlines())
                if nb_lines > 1:
                    ent = CodeEditor(wid, text=option_value)
                    ent.grid(row=1, column=0, sticky=tk.W+tk.E)
                else:  # it's just a line
                    ent = ttk.Entry(wid, justify='center')
                    ent.insert(0, option_value)
                    ent.grid(row=0, column=1, sticky=tk.W+tk.E)
                wid.get = lambda ent=ent: ent.get()
                wid.configure = ent.configure
            elif option_type is open:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                norm_path = normalized_path(option_value)
                but = FileButton(wid, **{('default_file' if os.path.exists(norm_path)
                                 else 'default_text'): norm_path or 'Pick a file'},
                                 on_file_change=self.update)
                but.grid(row=0, column=1, sticky=tk.E)
                wid.get = lambda but=but: but.get()
                wid.configure = but.configure
            elif ispartial(option_type, (tk.filedialog.askopenfilename, tk.filedialog.asksaveasfilename)):
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                norm_path = normalized_path(option_value) if option_value else option_value
                but = FileButton(wid, **{('default_file' if norm_path and os.path.exists(norm_path)
                                 else 'default_text'): norm_path or 'Pick a file'},
                                 on_file_change=self.update, tkcall=option_type)
                but.grid(row=0, column=1, sticky=tk.E)
                wid.get = lambda but=but: but.get()
                wid.configure = but.configure
            elif ispartialsubclass(option_type, tk.Widget):
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=0, column=0, sticky=tk.E)
                opt = option_type(master=wid)
                opt.grid(row=0, column=1, sticky=tk.E)
                if option_value is not None:
                    opt.set(option_value)
                wid.get = lambda opt=opt: opt.get()
                wid.configure = opt.configure
            else:
                raise ValueError("Unexpected option type {}.".format(option_type))
            wid.grid(row=1, column=idx, pady=5, padx=5)
            wid_options[option_name] = wid
            if option_desc:  ToolTip.on(wid, option_desc)
        if not self.script.options:  # no options to show
            self.label = tk.Label(self, text='no options available')
            self.label.grid(row=0, column=0, pady=5, padx=5)
        return wid_options


    @property
    def name(self) -> str: return self.NAME
    @property
    def description(self) -> str: return self.__doc__
    @property
    def tags(self) -> str: return self.TAGS
    @property
    def inputs(self) -> frozenset: return self.script.spec_inputs(**self.options)
    @property
    def outputs(self) -> frozenset: return self.script.spec_outputs(**self.options)
    @property
    def disabled(self) -> (str or bool): return self.DISABLED
    @property
    def erase_context(self) -> str: return self.ERASE_CONTEXT
    @property
    def active_by_default(self) -> str: return self.ACTIVE_BY_DEFAULT
    @property
    def ruleeditor(self) -> str: return self.script.ruleeditor


    @property
    def options(self):
        return {
            name: wid.get()
            for name, wid in self.wid_options.items()
        }

    def _run_on(self, context):
        """The reason why this widget exists: to run using the given context,
        and returning another context.

        """
        if self.script.input_mode is iter:
            context = clyngor.solve((), inline=context)
        return self.script.run_on(context, **self.options)
        return utils.join_on_genstr(self.script.run_on)(context, **self.options)


    def _state_change(self, state):
        """Will disable internal widgets if the Script is deactivated"""
        for wid in self.wid_options.values():
            wid.configure(state=state)

    def _open_sources(self):
        if self.script.source_view:
            return self.script.source_view(**self.options)
        else:
            return super()._source_view()

    def open_sources(self):
        if self.ruleeditor:
            self.master.replace_script(self, self.ruleeditor(
                master=None, name=self.name,
                tooltip=self.description,
                welcome_message=self.script.source_view(**self.options).strip()
            ))
        else:
            super().open_sources()
