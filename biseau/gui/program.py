# encoding: utf8
"""Definition of the frame aggregating all the scripts widget.

"""
import os
import time
import textwrap
import itertools
import tkinter as tk
from tkinter import filedialog

import clyngor
from biseau import core
from biseau.gui import ScriptWidget, UserCodeWidget, UserPythonCodeWidget, ToolTip, Runnable
from biseau.core import Script
from biseau.utils import normalized_path, join_on_genstr


DEFAULT_LP_FILE = 'out/out.lp'


class ProgramWidget(tk.LabelFrame):

    def __init__(self, master, clingo_bin_path:str=None, lp_file:str=DEFAULT_LP_FILE):
        self.grp_header = tk.Frame(master)
        self.txt_title = tk.StringVar(self.grp_header, 'ASP program')
        self.lab_title = tk.Label(self.grp_header, textvariable=self.txt_title)
        self.lab_title.grid(row=0, column=0)
        self.__lp_file = lp_file
        if clingo_bin_path:  # set the clingo path for clyngor itself
            self.__clingo_bin_path = normalized_path(clingo_bin_path)
            clyngor.CLINGO_BIN_PATH = self.__clingo_bin_path
        self.__clingo_bin_path = None
        self._source_code = '% No code generated yet.'

        super().__init__(master, labelwidget=self.grp_header)
        self.create_widgets()


    def create_widgets(self):
        self.populate_widgets_from_runnables([UserCodeWidget(self)])
        self.propagate_outputs()
        self.build_grid(first_time=True)


    def populate_widgets_from_runnables(self, runnables:iter, *, append:iter=()):
        """Build self.wids by placing the widgets generated from given scripts

        Override any other list that could be here before.
        append -- widgets to add at the end of the list

        """
        self.wids = list(runnables) + list(append)


    def update(self, origin:Runnable=None, user_input:bool=False):
        """
        origin -- the Runnable that ask the update (default: all)
        user_input -- whether of not the update comes from user_input
        """
        self.invalidate_cache_from_index(self.wids.index(origin) if origin else 0)
        if not user_input:  # the modification come from a script, not user input
            self.propagate_outputs()
        self.master.update()

    def invalidate_cache_from_index(self, index:int):
        """Will invalidate the cache of all scripts from given index (inclusive)
        to last index"""
        for wid in self.wids[index:]:
            wid.invalidate_cache()

    def invalidate_cache_from_script(self, script:Runnable):
        """Will invalidate the cache of all scripts from given one (inclusive)
        to last"""
        self.invalidate_cache_from_index(self.wids.index(script))

    def invalidate_cache(self):
        if any(self.wids): self.invalidate_cache_from_index(0)

    @property
    def runnables_names(self) -> iter:
        yield from (runnable.name for runnable in self.wids)


    def add_runnables(self, new_runnables:iter):
        """Instantiate and add given runnables at the end of the interface.

        new_runnables -- Runnable subclasses, or Script instances

        If a runnable is a Script instance, it will build
        it using a ScriptWidget.

        """
        for runnable in new_runnables:
            if isinstance(runnable, Runnable):
                self.wids.append(runnable.integrate(master=self))
            else:
                assert isinstance(runnable, Script), type(runnable)
                self.wids.append(ScriptWidget(self, runnable, active=True))
        self.sort_runnables()


    def replace_script(self, script, substitute:Runnable):
        """Replace given script by given substitute.
        """
        index = self.wids.index(script)
        script_to_delete = self.wids[index]
        self.wids[index] = substitute.integrate(master=self)
        script_to_delete.grid_forget()
        self.build_grid()


    def delete_grid(self):
        for wid in self.wids:
            wid.grid_forget()

    def build_grid(self, *, first_time:bool=False):
        """Will put the widgets in the right order, based on the self.wids list."""
        if not first_time:
            self.delete_grid()
        for row, wid in enumerate(self.wids):
            wid.grid(row=row, column=0, padx=5, pady=5, sticky=tk.W+tk.E)
        self.invalidate_cache_from_index(0)


    def spawn_user_input(self):
        """Spawn a UserCodeWidget at the very end of the program.

        """
        new_wid = UserCodeWidget(self)
        self.wids.append(new_wid)
        self.build_grid()
        self.update(new_wid)

    def spawn_python_user_input(self):
        """Spawn a UserCodeWidget at the very end of the program.

        """
        new_wid = UserPythonCodeWidget(self)
        self.wids.append(new_wid)
        self.build_grid()
        self.update(new_wid)


    def place_change_request(self, runnable:Runnable, move_up:bool=True):
        """Raise the runnable order.
        """
        index = self.wids.index(runnable)
        if index == 0 and move_up: return  # nothing to do
        if index == len(self.wids)-1 and not move_up: return  # nothing to do
        new_index = index + (-1 if move_up else 1)
        # swap them
        self.wids[index], self.wids[new_index] = self.wids[new_index], self.wids[index]
        # rebuild the grid
        self.build_grid()
        self.update()


    def move_up_request(self, runnable:Runnable):
        return self.place_change_request(runnable, move_up=True)
    def move_down_request(self, runnable:Runnable):
        return self.place_change_request(runnable, move_up=False)

    def close_script_request(self, script:Runnable):
        """Close given scriptwidget"""
        script.grid_forget()  # without that, it will keep waiting at it's old place
        self.wids.remove(script)
        self.build_grid()
        self.update()


    def scripts_before(self, script) -> iter:
        """Yield scripts that are before the given one in execution order"""
        for wid in self.wids:
            if wid is script: break
            yield wid

    def propagate_outputs_to_user_input(self, user_input):
        """Given user_input is the user_input widget to propagate to"""
        activated_scripts = self.activated_scripts(
            user_input=False,
            in_=self.scripts_before(user_input)
        )
        user_input.set_inputs(itertools.chain.from_iterable(
            script.outputs for script in activated_scripts
        ))

    def propagate_outputs(self):
        for wid in self.wids:
            if type(wid) is UserCodeWidget:
                self.propagate_outputs_to_user_input(wid)
            else:
                pass  # nothing to do, scripts don't care about previous scripts


    def activated_scripts(self, user_input:bool=True, *, in_=None) -> iter:
        """Return the currently active scripts, in all scripts or in the one given."""
        scripts_to_look_at = self.wids if in_ is None else in_
        yield from (
            wid for wid in scripts_to_look_at
            if wid.is_activated and (user_input or type(wid) is not UserCodeWidget)
        )


    def runnables_divided_by_orderability(self) -> (iter, iter):
        """Return (o, ¬o), where o is an iterable of all orderable runnables,
        and ¬o is an iterable of all non-orderable runnables.

        This last includes the UserCodeWidget instances.

        """
        is_orderable = lambda w: not isinstance(w, UserCodeWidget)
        is_non_orderable = lambda w: not is_orderable(w)

        def yield_if(cond:callable):
            yield from (wid for wid in self.wids if cond(wid))

        return yield_if(is_orderable), yield_if(is_non_orderable)


    def sort_runnables(self):
        """Sort the scripts so the lasts' inputs correspond to firsts' outputs.

        User inputs blocks will all be placed at the very end.
        A better way to go would be to detect their I/O based on code in it,
        and therefore allowing them to be sorted (by providing the
        inputs() and outputs() methods).

        """
        orderables, non_orderables = map(list, self.runnables_divided_by_orderability())
        orderables = list(core.scripts.merge_scripts_lists(orderables))
        self.delete_grid()
        self.populate_widgets_from_runnables(orderables, append=non_orderables)
        self.build_grid()


    def get_models(self) -> iter:
        """Return the models generated by scripts."""
        start = time.time()
        context = self._build_asp_source_code()
        with open(self.__lp_file, 'w') as fd:
            fd.write(context)
        if not context: return ()
        try:
            models = clyngor.solve(self.__lp_file, stats=False).by_predicate.int_not_parsed.careful_parsing.parse_args
        except FileNotFoundError as err:
            self.log("Ho ! Ho ! Ho ! Clingo is not in {e} !\nI am therefore"
                     " not able to find it. Sorry.\nBut if you put it"
                     " in {e}, everything will be alright."
                     "".format(e=self.__clingo_bin_path or 'your $PATH'))
            raise err
        try:
            models = tuple(models)
        except clyngor.ASPSyntaxError as err:
            self.log("Ouch ! Clingo said:\n{}".format('\n'.join(textwrap.wrap(str(err)))))
            models = ()
        runtime = time.time() - start
        self.txt_title.set('ASP program  ({} models found in {}s)'
                           ''.format(len(models), round(runtime, 3)))
        return models


    def _build_asp_source_code(self) -> str:
        """Build and return the ASP source code by visiting each active script"""
        context = ''  # begin empty
        for script in self.activated_scripts():
            func = join_on_genstr(script.run_on)
            if script.erase_context:
                context = func(context)
            else:
                context = func(context) + context
            if not context:
                self.log("Script {} nullifyied the context by returning "
                         "'{}'".format(script.name, context))
        self._source_code = context
        return self._source_code

    @property
    def asp_source_code(self) -> str:  return self._source_code

    def log(self, msg:str):
        return self.master.log(msg)

    @property
    def have_runnables(self) -> bool:
        return bool(self.wids)
