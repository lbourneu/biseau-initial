# encoding: utf8
"""Dialog allowing user to save a rule in memory.

"""


import os
import json
import shutil
import tkinter as tk
from tkinter import simpledialog
from tkinter import filedialog

from biseau.gui import SimpleCheckbox
from biseau.gui import utils


LOG_EXISTING_FILE = "⚠ The rule will be added to an existing set of rules"


class RuleSaverWindow(simpledialog.Dialog):

    def __init__(self, master, rule_content:str, filename_callback:callable,
                 language:str):
        self._rule_content = str(rule_content)
        self._file_choosen = False
        self._filename_callback = filename_callback
        self._language = str(language)
        super().__init__(master, title='Save a rule')

    def update_file_from_name(self, *_):
        if self._file_choosen: return  # avoid modification by changing the name
        name = self.txt_name.get().replace(' ', '_')
        fname = 'scripts/{}.json'.format(name)
        if os.path.exists(fname):
            self.log(LOG_EXISTING_FILE)
            self.but_file.configure(bg=utils.RUNNING_COLOR)
        else:
            self.log("")
            self.but_file.configure(bg=self._default_button_bg)
        self.txt_file.set(fname)

    def update_file(self):
        fname = tk.filedialog.asksaveasfilename(defaultextension='.json')
        if fname:
            self.txt_file.set(fname)
            self._file_choosen = True

    def log(self, msg:str):
        self.txt_log.set(msg)

    def body(self, master):
        WIDE = tk.W + tk.E
        grid_options = lambda r, c: {'sticky': tk.W + tk.E, 'padx': 10,
                                     'pady': 2, 'row': r, 'column': c}


        self.lab_name = tk.Label(master, text='Rule name:')
        self.txt_name = tk.StringVar(master, '')
        self.ent_name = tk.Entry(master, textvariable=self.txt_name)
        utils.add_write_callback(self.txt_name, self.update_file_from_name)
        self.lab_name.grid(**grid_options(0, 0))
        self.ent_name.grid(**grid_options(0, 1))

        self.lab_desc = tk.Label(master, text='Description:')
        self.txt_desc = tk.StringVar(master, '')
        self.ent_desc = tk.Entry(master, textvariable=self.txt_desc)
        self.lab_desc.grid(**grid_options(1, 0))
        self.ent_desc.grid(**grid_options(1, 1))

        self.lab_file = tk.Label(master, text='File:')
        self.txt_file = tk.StringVar(master, 'File to use')
        self.but_file = tk.Button(master, textvariable=self.txt_file, command=self.update_file)
        self._default_button_bg = self.but_file.config()['background'][-1]
        self.lab_file.grid(**grid_options(2, 0))
        self.but_file.grid(**grid_options(2, 1))

        self.lab_inputs = tk.Label(master, text='Inputs:')
        self.txt_inputs = tk.Entry(master)
        self.lab_outputs = tk.Label(master, text='Outputs:')
        self.txt_outputs = tk.Entry(master)
        self.lab_inputs.grid(**grid_options(3, 0))
        self.txt_inputs.grid(**grid_options(3, 1))
        self.lab_outputs.grid(**grid_options(4, 0))
        self.txt_outputs.grid(**grid_options(4, 1))

        self.chk_active = SimpleCheckbox(master, text='Active at startup', state=False)
        self.chk_active.grid(**grid_options(5, 0), columnspan=2)

        self.txt_log = tk.StringVar(master, ' ' * len(LOG_EXISTING_FILE))
        self.lab_log = tk.Label(master, textvariable=self.txt_log)
        self.lab_log.grid(**grid_options(6, 0), columnspan=2)


        return self.ent_name  # initial focus


    def apply(self):
        fname = self.txt_file.get()
        rule = {
            'name': self.txt_name.get(),
            'description': self.txt_desc.get().strip(),
            self._language: self._rule_content,
            'inputs': list(map(str.strip, self.txt_inputs.get().split())),
            'outputs': list(map(str.strip, self.txt_outputs.get().split())),
            'active at startup': self.chk_active.is_activated,
        }

        # if the file already contains data, we have to add data to it,
        #  instead of writing from scratch.
        if os.path.exists(fname):
            with open(fname, 'r') as fd:
                existing_rules = json.load(fd)
            if isinstance(existing_rules, dict):
                rule = [existing_rules, rule]
            elif isinstance(existing_rules, list):
                existing_rules.append(rule)
                rule = existing_rules

        # write the file, now we have the data
        with open(fname, 'w') as fd:
            json.dump(rule, fd, indent=4)
        self._filename_callback(fname)


    def validate(self) -> bool:
        """Log user's bad choices ; prevent apply in such case"""
        if not self.txt_name.get():
            self.log("A name is needed")
            return False
        if not self.txt_file.get():
            self.log("A file is needed")
            return False
        if not self.txt_desc.get():
            self.log("A description is needed")
            return False
        return True
