# encoding: utf8
"""Dialog allowing user to look at a rule content.

"""

import tkinter as tk
from tkinter import simpledialog


class SourceShowerWindow(simpledialog.Dialog):

    def __init__(self, master, source_to_show:str):
        self.__source = source_to_show
        super().__init__(master, title='Show content of a script')


    def body(self, master):
        WIDE = tk.W + tk.E
        grid_options = lambda r, c: {'sticky': tk.W + tk.E, 'padx': 10,
                                     'pady': 2, 'row': r, 'column': c}

        self.txt_source = tk.Text(master)
        self.txt_source.insert(1.0, self.__source)
        self.txt_source.configure(state='disabled')
        self.txt_source.grid(**grid_options(0, 0))

        # Bugfix for OSX. User can select the text despite the disabled state.
        self.txt_source.bind('<1>', lambda event: self.txt_source.focus_set())

        return self.txt_source  # initial focus


    def apply(self):
        pass  # nothing to do

    def validate(self) -> bool:
        return True  # nothing to do
