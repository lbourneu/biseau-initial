# encoding: utf8
"""The Runnable objects are the main components of Biseau.

"""

import time
import tkinter as tk
from functools import partial
from biseau import gui
from biseau.gui import ToolTip, utils, SimpleCheckbox


class Runnable(tk.LabelFrame):
    """Abstract Base class for all scripts to show in gui.

    The main work for subclasses is to define create_widgets() method.
    They may also want to write _edit_sources, which will allow user to edit
    the source shown by _open_sources.

    Note that a Runnable may be integrated into a GUI after its instanciation
    using the integrated() method.
    This allow runnables to be studied before integration, for instance
    into ScriptLoader.

    By default, attributes NAME, INPUTS, OUTPUTS, and TAGS are used
    to determine related knowledge.
    These attributes must be set.

    """

    def __init__(self, master=None, name:str=None, **integration_kwargs):
        if name: self.NAME = name
        self.master = master
        if master is not None:
            self.integrate(master, **integration_kwargs)
        else:
            self.integrate = partial(self.integrate, **integration_kwargs)

    def integrate(self, master, state:bool=None, tooltip:str='',
                  openable:bool=True, closable:bool=True):
        """Integrate the Runnable into master GUI"""
        group = self.create_header(master, self.name, state or True,
                                   openable, closable)
        super().__init__(master, labelwidget=group, bd=3, relief=tk.GROOVE)
        # The next call may lead to self.create_widgets run
        #  or a interface defined by the script itself.
        self.wid_options = self.create_widgets()
        self.invalidate_cache()
        # tooltip
        if not tooltip:
            output = self.outputs
            tooltip = "Yield " + ', '.join(output) if output else 'Yield nothing'
            ToolTip.on(self.chk_script, tooltip)
        return self  # for convenience

    def create_widgets(self) -> {str: tk.Widget}:
        if self.master is None:
            raise RuntimeError("Runnable has not been integrated in a GUI")
        return self._create_widgets()


    def update(self, *, user_input:bool=False):
        self.__cached_run_on = None
        self.master.update(origin=self, user_input=user_input)

    def invalidate_cache(self):
        self.__cached_run_on = None
        self.txt_runtime.set("NO CACHE")

    def state_change(self):
        state = 'normal' if self.is_activated else 'disabled'
        self._state_change(state)
        self.update()

    def _state_change(self, state):
        raise NotImplementedError("Subclasses should implement that")

    def _open_sources(self):
        raise NotImplementedError("Subclasses should overwrite this one")


    def create_header(self, master, name:str, state:bool, openable:bool, closable:bool):
        self.grp_header = tk.Frame(master, bd=3, relief=tk.GROOVE)
        # activation/deactivation + name
        self.chk_script = SimpleCheckbox(self.grp_header, text=name,
                                         state=bool(state),
                                         on_state_change=self.state_change)
        self.chk_script.grid(row=0, column=0)

        # label that indicate the last running time
        self._last_runtime = None
        self.txt_runtime = tk.StringVar(self.grp_header, self.last_runtime)
        self.lab_runtime = tk.Label(self.grp_header, textvariable=self.txt_runtime)
        self.lab_runtime.grid(row=0, column=1, padx=15)

        # buttons to move the runnable
        self.but_moveup = tk.Button(self.grp_header, text='↑',
                                    command=lambda: self.master.move_up_request(self))
        self.but_moveup.grid(row=0, column=2)
        ToolTip.on(self.but_moveup, "Move the script up, leading to an earlier execution")
        self.but_movedown = tk.Button(self.grp_header, text='↓',
                                      command=lambda: self.master.move_down_request(self))
        self.but_movedown.grid(row=0, column=3)
        ToolTip.on(self.but_movedown, "Move the script down, leading to a delayed execution")

        if openable:
            # button to open the runnable's inside
            self.but_open = tk.Button(self.grp_header, text='✍',
                                       command=lambda: self.open_sources())
            self.but_open.grid(row=0, column=4)
            ToolTip.on(self.but_open, "Open a window where you can see the source code")

        # button to invalidate the cache
        self.but_close = tk.Button(self.grp_header, text='I',
                                   command=lambda: self.master.invalidate_cache_from_script(self))
        self.but_close.grid(row=0, column=5)
        ToolTip.on(self.but_close, "Invalidate the cache of this script and the next ones")

        if closable:
            # button to close the runnable
            self.but_close = tk.Button(self.grp_header, text='X',
                                       command=lambda: self.master.close_script_request(self))
            self.but_close.grid(row=0, column=6)
            ToolTip.on(self.but_close, "Closes the script, leading to its disparition from the interface")


        return self.grp_header

    @property
    def last_runtime(self) -> str:
        if self._last_runtime is None:
            return '[NEVER RAN]'
        return '({}s)'.format(round(self._last_runtime, 3))


    @property
    def is_activated(self) -> bool:
        return self.chk_script.is_activated

    def run_start(self):
        """Get frame to be in RUNNING_COLOR"""
        self.chk_script.set_running_color()
        self._last_runtime = time.time()

    def run_stop(self):
        """Get frame to be in normal color"""
        self._last_runtime = time.time() - self._last_runtime
        self.chk_script.set_normal_color()

    def run_on(self, context) -> str:
        """The reason why this widget exists: to run using the given context,
        and returning another context.

        """
        if self.__cached_run_on is None:
            self.run_start()
            self.__cached_run_on = self._run_on(context)
            self.run_stop()
            self.txt_runtime.set(self.last_runtime)
        else:
            self.txt_runtime.set('[CACHED]')
        return self.__cached_run_on


    def open_sources(self):
        """Open a window, printing the content of the Runnable"""
        if self.editable_source:
            new_sources = None
            def callback(new): nonlocal new_sources ; new_sources = new
            gui.SourceEditorWindow(self, self._open_sources(), callback)
            if new_sources is not None:
                self.edit_sources(new_sources)
        else:
            gui.SourceShowerWindow(self, self._open_sources())

    def edit_sources(self, new:str):
        if self.editable_source:
            self._edit_sources(new)
            self.update()

    @property
    def editable_source(self) -> bool:
        return hasattr(self, '_edit_sources')

    def _source_view(self) -> str:
        """Called by open_source ; should be overwritten by subclasses"""
        return self.__cached_run_on or '% not yet generated'


    def spec_inputs(self) -> frozenset:
        return self.inputs()
    def spec_outputs(self) -> frozenset:
        return self.outputs()


    DISABLED = False
    ERASE_CONTEXT = False
    ACTIVE_BY_DEFAULT = False

    @property
    def name(self) -> str: return self.NAME
    @property
    def description(self) -> str: return self.__doc__
    @property
    def tags(self) -> str: return self.TAGS
    @property
    def inputs(self) -> frozenset: return self.INPUTS
    @property
    def outputs(self) -> frozenset: return self.OUTPUTS
    @property
    def disabled(self) -> (str or bool): return self.DISABLED
    @property
    def erase_context(self) -> str: return self.ERASE_CONTEXT
    @property
    def active_by_default(self) -> str: return self.ACTIVE_BY_DEFAULT
    @property
    def ruleeditor(self) -> str: return None
