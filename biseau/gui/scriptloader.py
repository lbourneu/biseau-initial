# encoding: utf8
"""Small dialog asking user which script(s) he wants to load
into the main gui.

"""

import os
import shutil
import itertools
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from tkinter import simpledialog
from biseau import core
from biseau.gui import SimpleCheckbox


TAG_ANY = 'all'


class ScriptLoaderWindow(simpledialog.Dialog):

    def __init__(self, master, base_dir:str, already_loaded_names:iter,
                 scripts_callback:callable):
        self.__base_dir = base_dir
        self.__already_loaded_names = frozenset(already_loaded_names)
        self.__scripts_callback = scripts_callback
        super().__init__(master, title='Script Loader')


    def detect_scripts(self) -> tuple:
        """Detect scripts in base dir, and list them"""
        scripts = core.scripts.build_all_scripts(base_dir=self.__base_dir)
        return sorted(tuple(scripts), key=lambda s: s.name)


    def body(self, master):
        self.__body_master = master
        self.create_widgets(self.__body_master)


    def create_widgets(self, master):
        WIDE = tk.W + tk.E
        self.__scripts = self.detect_scripts()
        all_tags = frozenset(itertools.chain.from_iterable(script.tags for script in self.__scripts))

        # build the tag chooser
        self.lab_tags = tk.Label(master, text='With tag:')
        self.lab_tags.grid(row=0, column=0, sticky=tk.E)
        self.txt_tags = tk.StringVar(master, TAG_ANY)
        self.cmb_tags = ttk.Combobox(master, textvariable=self.txt_tags,
                                     justify='left', state='readonly',
                                     values=sorted(all_tags))
        self.cmb_tags.bind('<<ComboboxSelected>>', lambda _:self.tag_filter_updated())
        self.cmb_tags.grid(row=0, column=1, sticky=tk.W+tk.E, padx=5)
        self.__target_tag = None if self.txt_tags.get() == TAG_ANY else self.txt_tags.get()

        # buttons
        self.but_all = tk.Button(master, text='Select all', command=self.select_all)
        self.but_all.grid(row=0, column=2, padx=2)
        self.but_invert = tk.Button(master, text='Invert selection', command=self.select_invert)
        self.but_invert.grid(row=0, column=3, padx=2)

        # build the group of scripts
        self.grp_scripts = tk.Frame(master)
        self.scripts_widgets = {}
        self.create_scripts_widgets()
        self.grp_scripts.grid(row=1, column=0, columnspan=4, sticky=tk.W+tk.E)
        return self.cmb_tags  # initial focus


    def create_scripts_widgets(self):
        """build a view (delete the previous one) for each found scripts"""
        for wid in self.scripts_widgets.values():
            wid.grid_forget()
            del wid
        self.scripts_widgets = {}
        wid = None
        for idx, script in enumerate(self.__scripts):
            if not self.validate_script(script):
                continue
            wid = self.build_widget_for(script, self.grp_scripts)
            wid.grid(row=idx, column=0, sticky=tk.W+tk.E, padx=5, pady=2)
            self.scripts_widgets[script] = wid


    def tag_filter_updated(self):
        self.__target_tag = (() if self.txt_tags.get() == TAG_ANY else self.txt_tags.get())
        self.create_scripts_widgets()


    def select_invert(self):
        for widget in self.scripts_widgets.values():
            widget.chk.toggle()
    def select_all(self):
        for widget in self.scripts_widgets.values():
            widget.chk.check()


    def validate_script(self, script) -> bool:
        """True iff given script must be printed at screen"""
        return all((
            script.name not in self.__already_loaded_names,  # don't load two times the same script
            self.__target_tag is None or (self.__target_tag in script.tags),
        ))

    def apply(self):
        choosen_scripts = frozenset(
            script for script, frame in self.scripts_widgets.items()
            if frame.chk.is_activated
        )
        self.__scripts_callback(choosen_scripts)


    def build_widget_for(self, script:'Script', master:tk.Widget) -> tk.Widget:
        chk_script = SimpleCheckbox(master, state=script.active_by_default,
                                    text=script.name)
        frame = tk.LabelFrame(master, labelwidget=chk_script, bd=3, relief=tk.GROOVE)
        description = tk.Label(frame, text=script.description)
        description.grid(sticky=tk.W + tk.E)
        frame.chk = chk_script
        frame.desc = description
        return frame
