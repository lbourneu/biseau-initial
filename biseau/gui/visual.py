# encoding: utf8
"""Widget showing to user an high level visual of the data.

"""

import os
import tkinter as tk

import PIL
from PIL import ImageTk

from biseau import gui
from biseau import core
from biseau.gui import ToolTip, utils


DEFAULT_VIZU_PATH = 'out/out.png'
MAX_IMAGE_HEIGHT = 600
MAX_IMAGE_WIDTH = 600
MODE_UNION = 'Union'
MODE_MULTI = 'Multishots'
SHOWMODE_IDLE = '  idle  '
SHOWMODE_WAIT = 'waiting…'


class VisualWidget(tk.LabelFrame):
    """Widget allowing user to see the result of its actions.

    See doc at http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/labelframe.html

    """
    def __init__(self, master=None, vizu_path:str=DEFAULT_VIZU_PATH,
                 vizu_height:int=MAX_IMAGE_HEIGHT, vizu_width:int=MAX_IMAGE_WIDTH):
        super().__init__(master=master, text='Visualization')
        self.vizu_path = str(vizu_path)
        self.dot_path = os.path.splitext(self.vizu_path)[0] + '.dot'
        self.vizu_height = vizu_height
        self.vizu_width = vizu_width
        self.create_widgets()
        self.__update()


    def create_widgets(self):
        self.viewer = gui.ImageViewer(self, self.vizu_path,
                                      self.vizu_width, self.vizu_height,
                                      load_at_init=False)
        self.viewer.pack(padx=5)

        self.grp_buts = tk.Frame(self)

        self.dot_source = '<Not currently generated>'
        self.but_dotsrc = tk.Button(self.grp_buts, text='dot', command=self.show_dot_source)
        self.but_dotsrc.grid(row=0, column=0)
        ToolTip.on(self.but_dotsrc, "Show the dot source code")

        self.txt_showmode = tk.StringVar(self.grp_buts)
        self.lab_showmode = tk.Label(self.grp_buts, textvariable=self.txt_showmode)
        self.lab_showmode.grid(row=0, column=1)
        self.set_showmode_idleing()
        ToolTip.on(self.lab_showmode, "Orange if the visualization is currently computed")

        self.lab_mode = tk.StringVar(self, MODE_UNION)
        self.but_mode = tk.Button(self.grp_buts, textvariable=self.lab_mode,
                                  command=self.switch_mode)
        self.but_mode.grid(row=0, column=3, sticky=tk.E+tk.W)
        ToolTip.on(self.but_mode, "Set the mode. Union is 'union of all models',"
                   "while Multishot is 'one image per model'.\n"
                   "Not Implemented (but you can push the button, it's safe).")

        self.chk_resize_x = gui.SimpleCheckbox(self.grp_buts, 'fit width', state=True,
                                               tooltip='make the visual fit the window width',
                                               on_state_change=self.__update)
        self.chk_resize_y = gui.SimpleCheckbox(self.grp_buts, 'fit height', state=True,
                                               tooltip='make the visual fit the window height',
                                               on_state_change=self.__update)
        self.chk_resize_x.grid(row=1, column=0, sticky=tk.E+tk.W)
        self.chk_resize_y.grid(row=1, column=1, sticky=tk.E+tk.W)

        self.grp_buts.pack()


    def update_from_visual_config(self, visual_config):
        """Will compute the image to show from given config, and effectively show it."""
        self.set_showmode_waiting()
        dot = core.dot_writer.one_graph_from_configs(visual_config)
        core.dot_writer.dot_to_png(dot, self.vizu_path, self.dot_path)
        self.__update()
        self.set_showmode_idleing()

    def __update(self):
        self.set_showmode_waiting()
        # TODO: gives control on the following options to user
        self.viewer.reload(max_height=None, max_width=None,
                           resize_width=self.chk_resize_x.get(), resize_height=self.chk_resize_y.get())
        self.set_showmode_idleing()

    def set_showmode_waiting(self):
        """Indicate that we are waiting for the update result"""
        self.txt_showmode.set(SHOWMODE_WAIT)
        self.lab_showmode.configure(fg=utils.COLOR_WAITING)
        self.update_idletasks()

    def set_showmode_idleing(self):
        """Indicate that we are NOT waiting for the update result"""
        self.txt_showmode.set(SHOWMODE_IDLE)
        self.lab_showmode.configure(fg=utils.COLOR_OK)
        self.update_idletasks()


    def switch_mode(self):
        if self.lab_mode.get() == MODE_UNION:
            self.lab_mode.set(MODE_MULTI)
        elif self.lab_mode.get() == MODE_MULTI:
            self.lab_mode.set(MODE_UNION)


    @property
    def current_mode(self): return self.lab_mode.get()


    def log(self, msg:str):  return self.master.log(msg)


    def show_dot_source(self):
        try:
            with open(self.dot_path) as fd:
                gui.SourceShowerWindow(self, fd.read())
        except FileNotFoundError:
            self.log("No dotfile found")
