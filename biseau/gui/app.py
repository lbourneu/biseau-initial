# encoding: utf8
"""Definition of the main GUI object.

"""
import os
import tkinter as tk
import threading
from biseau import gui
from biseau.gui import ToolTip, utils, ScriptLoaderWindow
from biseau import core


DEFAULT_WM_TITLE = 'Biseau: a new poc'
DEFAULT_SCRIPT_DIR = 'scripts'


class Application(tk.Frame):
    """Main GUI object, aggregating all others.

    """

    def __init__(self, master=None, script_dir:str=DEFAULT_SCRIPT_DIR,
                 clingo_bin_path:str=None):
        self._script_dir = str(script_dir)
        self.__clingo_bin_path = clingo_bin_path
        self.__thread_viz = None
        super().__init__(master or tk.Tk())
        self.master.wm_title(DEFAULT_WM_TITLE)
        self.master.minsize(1400, 900)
        self.master.resizable(True, True)
        self.create_widgets()


    def create_widgets(self):
        self.wid_visual = gui.VisualWidget(master=self)
        self.wid_visual.grid(row=0, column=0, padx=15, pady=15)

        self.wid_program = gui.ProgramWidget(master=self, clingo_bin_path=self.__clingo_bin_path)
        self.wid_program.grid(row=0, column=1, rowspan=2, padx=15, pady=15, sticky=tk.E+tk.W)

        self.grp_buttons = self.create_buttons()
        self.grp_buttons.grid(row=1, column=0, padx=15, pady=15, sticky=tk.E+tk.W)

        self.frm_log = tk.LabelFrame(self, text='logs')
        self.txt_log = tk.StringVar(self, '')
        self.lab_log = tk.Label(self.frm_log, textvariable=self.txt_log,
                                font=utils.ERROR_FONT(self), fg=utils.COLOR_ERR)
        self.lab_log.pack(expand=True)
        self.frm_log.grid(row=2, column=0, columnspan=2, padx=5, pady=5, sticky=tk.E+tk.W)

        self.grid()


    def create_buttons(self):
        grid_options = lambda r, c: {'sticky': tk.W + tk.E, 'padx': 10,
                                     'pady':5, 'row': r, 'column': c}
        grp_buttons = tk.LabelFrame(self, text='Various buttons')

        ## First row
        self.but_sort = tk.Button(grp_buttons, text='Sort', command=self.wid_program.sort_runnables)
        ToolTip.on(self.but_sort, "Sort the scripts according to their I/O")
        self.but_sort.grid(**grid_options(0, 0))

        self.but_spawnscript = tk.Button(grp_buttons, text='Spawn script', command=self.spawn_new_scripts)
        self.but_spawnscript.bind('<Button-3>', lambda _: self.spawn_new_scripts(with_doublons=True))
        ToolTip.on(self.but_spawnscript, "Add new scripts. Use Right-click to allow doublons")
        self.but_spawnscript.grid(**grid_options(0, 1))

        self.but_spawnuserinput = tk.Button(grp_buttons, text='Spawn text',
                                            command=self.wid_program.spawn_user_input)
        ToolTip.on(self.but_spawnuserinput, "Add a new text zone for coding")
        self.but_spawnuserinput.grid(**grid_options(0, 2))

        self.but_spawnuserinput = tk.Button(grp_buttons, text='Spawn pytext',
                                            command=self.wid_program.spawn_python_user_input)
        ToolTip.on(self.but_spawnuserinput, "Add a new text zone for coding in python")
        self.but_spawnuserinput.grid(**grid_options(0, 3))

        self.but_run = tk.Button(grp_buttons, text='Run', command=self.update)
        ToolTip.on(self.but_run, "Will run each script from top to bottom, "
                   "giving to the next the output of the previous.\n"
                   "Will also update the visualization")
        self.but_run.grid(**grid_options(0, 4))

        ## Second row
        self.but_export = tk.Button(grp_buttons, text='Export', command=self.export)
        ToolTip.on(self.but_export, "Export code and visuals to files")
        self.but_export.grid(**grid_options(1, 0))
        self.but_help = tk.Button(grp_buttons, text='Help')
        ToolTip.on(self.but_help, "Sorry, User. I'm afraid i can't do that.")
        self.but_help.grid(**grid_options(1, 1))
        self.but_logs = tk.Button(grp_buttons, text='Clear logs', command=lambda: self.log(''))
        ToolTip.on(self.but_logs, "Did they bother you ?")
        self.but_logs.grid(**grid_options(1, 2))
        self.but_cach = tk.Button(grp_buttons, text='Clear cache',
                                  command=self.wid_program.invalidate_cache)
        ToolTip.on(self.but_cach, "Invalidate the cache for all scripts")
        self.but_cach.grid(**grid_options(1, 3))
        self.but_quit = tk.Button(grp_buttons, text='Quit', command=self.quit)
        ToolTip.on(self.but_quit, "For a reason i can't explain, peoples LOVE"
                   " to have a 'quit' button")
        self.but_quit.grid(**grid_options(1, 4))

        return grp_buttons


    def update(self):
        models = self.wid_program.get_models()
        if not models:
            if self.wid_program.have_runnables:
                self.log("No model generated. There is something wrong with a script.\n"
                         "(and i could probably tell you which one if it was so simple…)")
            else:
                self.log("…")
            return
        cfg = core.visual_config_from_asp(models)
        self.log("Computation finished")
        if self.__thread_viz:
            self.__thread_viz.join()
        self.__thread_viz = threading.Thread(target=self.wid_visual.update_from_visual_config, args=[cfg]).start()



    def spawn_new_scripts(self, *, with_doublons:bool=False):
        scripts = None
        already_loaded = () if with_doublons else self.wid_program.runnables_names
        def callback(new_scripts): nonlocal scripts ; scripts = new_scripts
        ScriptLoaderWindow(self, self._script_dir, already_loaded, callback)
        if scripts:
            self.log("{} script{} added".format(len(scripts), 's' if len(scripts) > 1 else ''))
            self.wid_program.add_runnables(scripts)
        else:
            self.log("No script added")


    def export(self):
        gui.exporter.ExporterWindow(self, self.wid_program.asp_source_code,
                                    'out/out.png', 'out/out.dot')

    def log(self, msg:str):
        """Write given message in logs"""
        self.txt_log.set(msg)
        self.update_idletasks()


    @staticmethod
    def run(**kwargs):
        """Instanciate and run the main application"""
        win = Application(**kwargs)
        win.mainloop()
