# encoding: utf8
"""Dialog allowing user to look at a rule content.

"""

import tkinter as tk
from tkinter import simpledialog


LOG_MODIFIED_SOURCE = "⚠ The source have been modified"


class SourceEditorWindow(simpledialog.Dialog):

    def __init__(self, master, source_to_show:str, callback_source:callable=None):
        self.__source = source_to_show
        self.__callback = callback_source
        super().__init__(master, title='Allow edition of a script')


    def body(self, master):
        WIDE = tk.W + tk.E
        grid_options = lambda r, c: {'sticky': tk.W + tk.E, 'padx': 4,
                                     'pady': 2, 'row': r, 'column': c}

        self.txt_source = tk.Text(master, state='normal')
        self.txt_source.insert(1.0, self.__source)
        self.__old_text = self.current_text

        self.txt_source.grid(**grid_options(0, 0))
        self.txt_source.bind('<KeyRelease>', lambda _: self.text_modified())

        self.txt_log = tk.StringVar(self, '')
        self.lab_log = tk.Label(master, textvariable=self.txt_log)
        self.lab_log.grid(**grid_options(1, 0))

        return self.txt_source  # initial focus


    @property
    def current_text(self) -> str:
        return self.txt_source.get(1.0, tk.END)

    def text_modified(self):
        if self.current_text == self.__old_text:
            self.txt_log.set('')
        else:
            self.txt_log.set(LOG_MODIFIED_SOURCE)

    def apply(self):
        """Will send the new sources to callback"""
        if self.__callback:
            self.__callback(self.txt_source.get(1.0, tk.END))

    def validate(self) -> bool:
        return True  # nothing to do
