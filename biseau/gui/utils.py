# encoding: utf8
"""Various GUI-related helpers.

"""


import os
import tkinter as tk
from tkinter import font
from tkinter import filedialog
from biseau import utils

from biseau.gui import ToolTip


RUNNING_COLOR = '#fd6b14'
COLOR_WAITING = RUNNING_COLOR
COLOR_ERR = 'red'
COLOR_OK = 'green'
ERROR_FONT = lambda master: font.Font(master, family='TkFixedFont', size=10, weight='bold')

USER_TEXT_COLUMNS, USER_TEXT_LINES = 50, 6
USER_TEXT_FONTSIZE = 10
FAVORITE_FONT_FAMILIES = 'Source Code Pro', 'Source Sans Pro'


def add_write_callback(widget, callback:callable):
    """This is tkinter related. Two ways to do the same thing.
    The new one is not available everywhere.
    """
    try:  # first try the new way
        widget.trace_add("write", callback)
    except AttributeError:  # then the deprecated one
        widget.trace("w", callback)


class SimpleCheckbox(tk.Frame):
    """A checkbox able to say efficiently if it is activated or not,
    using its is_activated property.

    """

    def __init__(self, parent, text:str, state:bool,
                 on_state_change:callable=None, tooltip:str=None):
        tk.Frame.__init__(self, parent)
        self.create_widgets(state, text, tooltip)
        self.on_state_change = on_state_change

    def create_widgets(self, initial_value:bool, text:str, tooltip:str):
        self.value = tk.IntVar(value=initial_value)
        self.w_check = tk.Checkbutton(self, var=self.value, text=text)
        add_write_callback(self.value, lambda *_: self.state_changed())
        self.w_check.pack()
        self._normal_color = self.w_check.config()['background'][-1]
        if tooltip: ToolTip.on(self, tooltip)

    @property
    def is_activated(self) -> bool:
        return bool(self.value.get())

    def get(self) -> bool:
        return self.is_activated

    def state_changed(self):
        if self.on_state_change:
            self.on_state_change()

    def set_running_color(self):
        self.w_check.configure(bg=RUNNING_COLOR)
        self.w_check.update_idletasks()  # redraw (do not wait for the end of event handling)
    def set_normal_color(self):
        self.w_check.configure(bg=self._normal_color)
        self.w_check.update_idletasks()  # redraw (do not wait for the end of event handling)

    def configure(self, state):
        self.w_check.configure(state=state)

    def check(self) -> bool:
        self.value.set(True)
        return self.is_activated
    def uncheck(self) -> bool:
        self.value.set(False)
        return self.is_activated
    def toggle(self) -> bool:
        self.value.set(not self.is_activated)
        return self.is_activated


class FileButton(tk.Frame):
    """This widget is a button allowing user to choose a file,
    and that show him which file he chooses.

    """

    def __init__(self, parent, default_extension:str=None,
                 default_file:str=None, default_text:str=None,
                 on_file_change:callable=None,
                 tkcall:callable=tk.filedialog.askopenfilename):
        """
        default_extension -- extension to look at
        default_file -- the file initially choosen
        default_text -- the text to show in the button, without select any file
        """
        if default_file and not os.path.exists(default_file):
            raise ValueError("Given file '{}' do not exist".format(default_file))
        elif not default_file and not default_text:
            raise ValueError("A default file or a default text must be given")
        elif default_file and default_text:
            raise ValueError("A default file XOR a default text must be given")

        tk.Frame.__init__(self, parent)
        self.create_widgets()
        self.__tkcall = tkcall
        self.__default_ext = default_extension
        self.__on_file_change = on_file_change
        self.__max_txt_size = 20

        self.__current_filename = None
        if default_file:
            self.select_file(default_file, do_callback=False)
        else:
            self.set_button_text(default_text)


    def create_widgets(self):
        self.txt_butfile = tk.StringVar('')
        self.wid_butfile = tk.Button(self, textvariable=self.txt_butfile,
                                     command=self.select_file)
        self.wid_butfile.pack()


    def select_file(self, filename:str=None, *, do_callback:bool=True):
        prev_file = self.__current_filename
        if not filename:
            if self.__default_ext:
                filename = self.__tkcall(defaultextension=self.__default_ext)
            else:
                filename = self.__tkcall()
            if not filename:
                return  # abort the modifications
        self.__current_filename = utils.normalized_path(filename)
        if self.__current_filename != prev_file and do_callback:
            self.__on_file_change()
        text = os.path.relpath(self.__current_filename)
        if len(text) > self.__max_txt_size:
            text = text[len(text) - self.__max_txt_size:]
        self.set_button_text(text)

    def set_button_text(self, text:str):
        self.txt_butfile.set(text)

    def get(self) -> str:
        return self.__current_filename

    def configure(self, state):
        self.wid_butfile.configure(state=state)


class CodeEditor(tk.Frame):

    def __init__(self, parent, text:str, on_state_change:callable=None, tooltip:str=None):
        tk.Frame.__init__(self, parent)
        self.__on_state_change = on_state_change
        self.__welcome_message = str(text)
        self.create_widgets(tooltip)


    def create_widgets(self, tooltip:str or None):
        # Text where user can write its own rules.
        # print('FAMILIES:', 'TkFixedFont' in font.families(), font.families())
        font_family = 'TkFixedFont'
        for wanted_family in FAVORITE_FONT_FAMILIES:
            if wanted_family in font.families():
                font_family = 'Source Code Pro'
                break
        code_font = font.Font(family=font_family, size=USER_TEXT_FONTSIZE)
        nb_lines = len(self.__welcome_message.splitlines())
        self.frm_text = tk.Frame(self)
        self.wid_text = tk.Text(self.frm_text, width=USER_TEXT_COLUMNS, height=nb_lines)
        self.wid_text.configure(font=code_font)
        if self.__on_state_change:
            self.wid_text.bind('<KeyRelease>', lambda _: self.__on_state_change())
        self.wid_text.insert(tk.END, self.__welcome_message)
        self.wid_text.pack(side='left', fill='both', expand=True, padx=5, pady=3)
        # self.wid_text.grid(row=0, column=0, sticky=tk.W+tk.E, padx=5, pady=3)
        # put a vertical scrollbar
        self.scb_text = tk.Scrollbar(self.frm_text, command=self.wid_text.yview, orient=tk.VERTICAL)
        self.wid_text.configure(yscrollcommand=self.scb_text.set)
        self.scb_text.pack(side='right', fill='y')
        self.wid_text.xview_moveto(0)
        self.wid_text.yview_moveto(0)
        self.frm_text.grid(row=0, column=0, sticky=tk.W+tk.E, padx=5, pady=3)

        if tooltip:
            ToolTip.on(self.wid_text, tooltip)


    def get(self) -> str:
        return self.wid_text.get(1.0, tk.END)

    def configure(self, *args, **kwargs):
        self.wid_text.configure(*args, **kwargs)
