# encoding: utf8
"""Small dialog asking user where to export all data.

"""

import os
import shutil
import tkinter as tk
from tkinter import simpledialog
from tkinter import filedialog
from biseau.gui import SimpleCheckbox


class ExporterWindow(simpledialog.Dialog):
    __doc__

    def __init__(self, master, asp_code:str, png_file:str, dot_file:str):
        self._asp_code = str(asp_code)
        self._png_file = str(png_file)
        self._dot_file = str(dot_file)
        super().__init__(master, title='Exporter')


    def body(self, master):
        WIDE = tk.W + tk.E
        self.chk_png = SimpleCheckbox(self, state=True,
                                      text='export visualization in PNG')
        self.chk_dot = SimpleCheckbox(self, state=True,
                                      text='export visualization in dot')
        self.chk_asp = SimpleCheckbox(self, state=True,
                                      text='export ASP source code')
        self.chk_png.pack(anchor=tk.W)
        self.chk_dot.pack(anchor=tk.W)
        self.chk_asp.pack(anchor=tk.W)

        return self.chk_png  # initial focus


    def apply(self):
        fname = tk.filedialog.asksaveasfilename(defaultextension='')
        if fname:
            base, ext = os.path.splitext(fname)
            assert os.path.exists(os.path.split(base)[0]), os.path.split(base)[0]
            self.export(base, self.chk_png.is_activated,
                        self.chk_dot.is_activated, self.chk_asp.is_activated)


    def export(self, basename:str, png:bool, dot:bool, asp:bool):
        if asp:
            with open(basename + '.lp', 'w') as fd:
                fd.write(self._asp_code)
        if png:
            shutil.copy(self._png_file, basename + '.png')
        if dot:
            shutil.copy(self._dot_file, basename + '.dot')
