"""Definition of a tk image viewer"""

import os
import tkinter as tk

import PIL
from PIL import ImageTk


class ImageViewer(tk.Frame):

    def __init__(self, master, imagefile:str, width:int, height:int,
                 background='#ffffff', load_at_init:bool=True):
        super().__init__(master or tk.Tk())
        self.imagefile = imagefile
        self.maxheight = height
        self.maxwidth = width
        self.__on_drag = False
        self.__background = background
        self._create_widgets()
        if load_at_init:
            self.reload()


    def _create_widgets(self):
        # the container of the whole thing
        self.canvas = tk.Canvas(self, background=self.__background,
                                borderwidth=0, highlightthickness=0,
                                width=self.maxwidth,
                                height=self.maxheight)

        # the real container, and the scrollbars
        self.vsb = tk.Scrollbar(self, orient='vertical', command=self.canvas.yview)
        self.hsb = tk.Scrollbar(self, orient='horizontal', command=self.canvas.xview)
        self.canvas.configure(xscrollcommand=self.hsb.set, yscrollcommand=self.vsb.set)

        self.vsb.pack(side='right', fill='y')
        self.hsb.pack(side='bottom', fill='x')
        self.canvas.pack(anchor='nw', fill='both', expand=True)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # self.canvas.bind("<Configure>", lambda _: self._on_canvas_configure())
        self.canvas.bind('Left', lambda _: self._move_left())
        # help(self.canvas)
        # help(self.canvas.bind)
        self.canvas.bind('<ButtonPress-1>', self._start_drag, add=True)
        self.canvas.bind('<ButtonRelease-1>', self._stop_drag, add=True)
        self.canvas.bind('<Motion>', self._do_drag, add=True)
        # TODO: how it works ? Why it doesn't with other sequence than <Mouse-1> ?
        self.canvas.tag_bind('image', '<Control-A>', lambda _: self._move_left(), add=True)
        self.pack(expand=True)


    def _move_left(self):
        raise NotImplementedError("You succeed to get keyboard event detection working.")

    def _start_drag(self, event):
        self.__on_drag = event.x, event.y
    def _stop_drag(self, event):
        self.__on_drag = None
    def _do_drag(self, event):
        if self.__on_drag:
            x, y = self.__on_drag
            self.__on_drag = event.x, event.y
            dx, dy = event.x - x, event.y - y
            if dx != 0:
                self.canvas.xview_scroll(dx, 'units')
            if dy != 0:
                self.canvas.yview_scroll(dy, 'units')



    def _on_canvas_configure(self):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))


    def reload(self, **kwargs):
        self._retrieve_image(**kwargs)
        self._show_image()


    def _show_image(self):
        """Set the image in path as value of the image label"""
        center = self.maxwidth // 2, self.maxheight // 2
        self.canvas.create_image(*center, image=self.data, tag='image')
        self._on_canvas_configure()


    def _retrieve_image(self, max_height:int=None, max_width:int=None,
                        resize_width:bool=False, resize_height:bool=False):
        """Set the image in path as value of the image label"""
        try:
            image = PIL.Image.open(self.imagefile)
        except (FileNotFoundError, OSError):
            image = PIL.Image.new('RGB', (self.image_maxwidth, self.image_maxheight), color='white')

        if resize_width and not max_width:
            max_width = self.maxwidth
        if resize_height and not max_height:
            max_height = self.maxheight
        if max_height and image.height > max_height:
            size_ratio = max_height / image.height
            new_width = int(size_ratio * image.width)
            if max_width and new_width > max_width:
                size_ratio = max_width / new_width
                new_height = int(size_ratio * max_height)
                new_width = int(size_ratio * new_width)
            else:
                new_height = max_height
            image = image.resize((new_width, new_height), PIL.Image.ANTIALIAS)
        self.data = PIL.ImageTk.PhotoImage(image)


    @staticmethod
    def run(*args, **kwargs):
        """Instanciate and run the main application"""
        win = ImageViewer(*args, **kwargs)
        win.mainloop()


if __name__ == "__main__":
    ImageViewer.run(tk.Tk(), 'exemple.png', 200, 100)
