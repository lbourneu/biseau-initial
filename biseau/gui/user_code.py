# encoding: utf8
"""Widget allowing user to write its own code
and run details.

"""

import os
import itertools
import tkinter as tk
from tkinter import font
from biseau import core, gui
from biseau.gui import ToolTip, SimpleCheckbox, Runnable, RuleSaverWindow, CodeEditor


HEADER_LINE, INPUTS_LINE = '{} INPUTS:\n', '{}   - '
EDITOR_WELCOME_MESSAGE = r"""% Create your own rules here.
link(a,b).
color(a,b,green).
color(a,red).
shape(b,rectangle).


"""


class UserCodeWidget(Runnable):

    def __init__(self, master, name:str="Write your own {language}", *,
                 comment_char:str='%', rule_char:str='.', rule_per_line:int=4,
                 welcome_message:str=EDITOR_WELCOME_MESSAGE,
                 language:str='ASP', openable:bool=False,
                 tooltip:str="See user documentation for a tutorial"):
        self.rule_per_line = int(rule_per_line)
        self.__rule_char = str(rule_char)
        self.__language = str(language)
        self.__comment_char = str(comment_char)
        self.__welcome_message = str(welcome_message)
        super().__init__(master, name=name.format(language=self.__language),
                         state=True, tooltip=tooltip, openable=openable)


    def _state_change(self, state):
        """Will disable internal widgets if the Script is deactivated"""
        for wid in (self.wid_user_rule, *self.grp_run.children.values()):
            wid.configure(state=state)


    def create_widgets(self):
        # Text where user can write its own rules.
        self.wid_user_rule = CodeEditor(self, text=self.__welcome_message,
                                        on_state_change=self.letter_written_by_user)
        self.wid_user_rule.grid(row=0, column=0, columnspan=5, sticky=tk.W+tk.E, padx=5, pady=3)
        self.number_of_dot = self.__welcome_message.count('.')
        self.number_of_percent = self.__welcome_message.count(self.__comment_char)

        # (auto-)run buttons
        self.grp_run = tk.Frame(self)

        self.txt_models_count = tk.StringVar('')
        self.lab_models_count = tk.Label(self.grp_run, textvariable=self.txt_models_count)
        self.lab_models_count.grid(row=1, column=1, padx=15, sticky=tk.W)
        ToolTip.on(self.lab_models_count, "Number of models found by the solver")

        self.but_run = tk.Button(self.grp_run, text='Run', command=self.update)
        self.but_run.grid(row=1, column=2, padx=15, pady=10, sticky=tk.E)
        ToolTip.on(self.but_run, "Will run each script from top to bottom,"
                   "giving to the next the output of the previous. "
                   "Will also update the visualization")

        self.but_autorun = SimpleCheckbox(self.grp_run, text='auto-run', state=True)
        self.but_autorun.grid(row=1, column=3, padx=15, pady=10, sticky=tk.W+tk.E)
        ToolTip.on(self.but_autorun, "An heuristic will try to infer the best "
                   "moment to run automatically the program. "
                   "You may find that helpful")

        self.but_header = SimpleCheckbox(self.grp_run, text='show inputs',
                                         state=False, on_state_change=self.update_header)
        self.but_header.grid(row=1, column=4, padx=15, pady=10, sticky=tk.W+tk.E)
        ToolTip.on(self.but_header, "Will show in text the detected inputs for the script")

        self.but_asrule = tk.Button(self.grp_run, text='Save as rule',
                                    command=self.convert_to_rule)
        self.but_asrule.grid(row=1, column=5, padx=15, pady=10, sticky=tk.W+tk.E)
        ToolTip.on(self.but_asrule, "Prompt about some info, and save these lines as a rule.")

        self.grp_run.grid(row=1, column=0, columnspan=self.rule_per_line)


    def convert_to_rule(self):
        """Prompt user in order to build a valid JSON rule, and save it into
        a dedicated file.

        """
        fname = None
        def filename_callback(user_choice): nonlocal fname ; fname = user_choice
        RuleSaverWindow(self, self.user_rule, filename_callback, language=self.__language)
        if not fname: return
        scripts = tuple(core.scripts.build_scripts_from_json_file(fname))
        if len(scripts) > 0:
            new_script = scripts[-1]
        else:
            raise IndexError("Invalid index: {} found rules, index requested is {}"
                             "".format(len(scripts), index))
        self.master.replace_script(self, gui.ScriptWidget(self, new_script))


    @property
    def user_rule(self):
        """Return the source code written by user"""
        return self.wid_user_rule.get()


    def letter_written_by_user(self):
        """If there is a different number of dot or % than before,
        a compilation must be ran.

        TODO: this is enough for poc, but a better detection of interesting
        changes could be done, by monitoring what the insertion really do
        (commenting a populated line would be a reason to re-run the computations.
        Commenting an empty line is not. Adding a dot in comments is not.)
        It could be not that difficult to just parse the comment space,
        and count characters out of comments. A change due to '.' or '%'
        insertion/deletion would be a valid reason to re-run the computations.

        """
        current_dots = self.user_rule.count(self.__rule_char)
        current_percents = self.user_rule.count(self.__comment_char)
        if self.number_of_dot != current_dots or self.number_of_percent != current_percents:
            if self.but_autorun.is_activated:
                self.update()
        self.number_of_dot = current_dots
        self.number_of_percent = current_percents

    def _run_on(self, context:str):
        return context + '\n' + self.user_rule


    def update(self):
        super().update(user_input=True)


    def set_inputs(self, inputs:iter):
        """Update the text field with a header listing all given inputs."""
        # Build the new header, and update the text
        self.header_lines = (
            HEADER_LINE.format(self.__comment_char) +
            ''.join(INPUTS_LINE.format(self.__comment_char, input) for input in inputs)
        )
        self.update_header()

    def header_lines_in_text(self) -> iter:
        """Yield the header lines found in text widget"""
        lines = iter(self.user_rule.splitlines(True))
        first = next(lines)
        if first == HEADER_LINE.format(self.__comment_char):
            yield first
            for line in lines:
                if line.startswith(INPUTS_LINE.format(self.__comment_char)):
                    yield line
                else:  # now, it's no header. The remaining lines must be kept
                    lines = itertools.chain([line], lines)
                    break
        else:  # there is no header
            lines = iter(self.user_rule.splitlines(True))

    def update_header(self):
        """Modify the text so the header will (or not if deactivated) appear"""
        # delete any previous header
        first_non_header_line = sum(1 for line in self.header_lines_in_text())
        lines = self.user_rule.splitlines(True)[first_non_header_line:]
        self.wid_user_rule.wid_text.delete(1.0, tk.END)
        self.wid_user_rule.wid_text.insert(1.0, ''.join(lines))
        # put the new header, if any
        if self.header_lines and self.but_header.is_activated:
            self.wid_user_rule.wid_text.insert(1.0, self.header_lines)
