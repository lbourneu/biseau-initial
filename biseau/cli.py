# encoding: utf8
"""Biseau's CLI

"""

import os
import argparse
from biseau import gui


def parse_args(args:iter=None) -> dict:
    return cli_parser().parse_args(args)


def cli_parser() -> argparse.ArgumentParser:
    """Return the dict of options set by CLI"""

    # main parser
    parser = argparse.ArgumentParser(description=__doc__.strip())
    subs = parser.add_subparsers(dest='mode')

    pargui = subs.add_parser('gui', description='Launch the gui')
    pargui.add_argument('--clingo', default=None, type=existant_file,
                        help="Path to clingo's binary")
    parser.add_argument('--script-dir', '-s', nargs='+', metavar='DIR',
                        default=gui.DEFAULT_SCRIPT_DIR, type=existant_file,
                        help="Directories containing JSON or Python scripts")

    parcli = subs.add_parser('cli', description='Run the CLI')
    parcli.add_argument('aspfiles', nargs='+', metavar='FILE', type=existant_file,
                        help="Path to ASP files to run")
    parcli.add_argument('pngfile', type=str,
                        help=r"Path to png file to write ; {} for one file per viz level")
    parcli.add_argument('--dotfile', type=str, default=None,
                        help=r"Path to dot file to write ; {} for one file per viz level")

    return parser


def existant_file(filepath:str) -> str:
    """Argparse type, raising an error if given file does not exists"""
    if not os.path.exists(filepath):
        raise argparse.ArgumentTypeError("file {} doesn't exists".format(filepath))
    return filepath
