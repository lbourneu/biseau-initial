# encoding: utf8
from biseau import cli, gui, core
import clyngor


def run_gui(**kwargs):
    """Also used as an entry point"""
    gui.Application.run(**kwargs)


if __name__ == "__main__":
    args = cli.parse_args()
    # print(args)

    if args.mode == 'gui':
        run_gui(script_dir=args.script_dir, clingo_bin_path=args.clingo)
    else:
        models = clyngor.solve(args.aspfiles).by_predicate
        configs = core.visual.visual_config_from_asp(models)
        dotlines = core.dot_writer.one_graph_from_configs(configs)
        if args.dotfile:
            core.dot_writer.dot_to_png(dotlines, args.pngfile, args.dotfile)
        else:
            core.dot_writer.dot_to_png(dotlines, args.pngfile)
