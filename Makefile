
gui:
	python -m biseau gui
cli:
	python -m biseau cli data/cli_usage_example.lp out/cliout.png --dotfile out/cliout.dot
help:
	python -m biseau --help

.PHONY: gui cli t tests install_deps


t: tests
tests:
	python -m pytest test biseau --doctest-module -vv


install_deps:
	python -c "import configparser; c = configparser.ConfigParser(); c.read('setup.cfg'); print(c['options']['install_requires'])" | xargs pip install -U

clear:
	- rm -r build/ .cache biseau.egg-info
