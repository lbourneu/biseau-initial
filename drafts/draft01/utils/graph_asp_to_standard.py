"""Convert an asp file with edge/2 atoms
into slf/cxt file, needed for instance by lattice miner or inclose
algorithm implementation.

The slf/cxt file will be overwritten.

For the complete scripts with CLI and all,
see https://github.com/aluriak/dotfiles/tree/master/scripts/graph_asp_to_standard.py

"""
import os
import sys
import itertools
from collections import namedtuple, defaultdict
import clyngor

Context = namedtuple('Context', 'objects, attributes, relations')
# objects -- tuple of objects
# attributes -- tuple of attributes
# relations -- frozenset of holding (object, attribute)


def extract_context(input_file:str, names:[str]=('edge', 'rel'),
                    symetric:bool=False, anonymize:bool=False,
                    keep_quotes:bool=False, prefixed:bool=False) -> Context:
    model = next(clyngor.solve(input_file).careful_parsing.by_predicate)
    relations = model.get('edge', model.get('rel'))
    if not relations:
        raise ValueError("Given file ({}) do not contains edge/2 or rel/2, but {}"
                         "".format(input_file, ', '.join(links)))

    assert all(len(t) == 2 for t in relations)

    # format the identifiers
    formatted = lambda s: s.strip('"')
    if keep_quotes:
        formatted = lambda s: s.strip('"')
    if anonymize:
        gen_ids = itertools.count(1)
        ids = defaultdict(lambda: next(gen_ids))
        formatted = lambda s: str(ids[s])

    relations = (
        (formatted(obj), formatted(att))
        for obj, att in relations
    )

    with_prefix = lambda o, a: (o, a)
    if prefixed:
        with_prefix = lambda o, a: ('o' + o, 'a' + a)

    if symetric:
        relations = frozenset(
            with_prefix(ext, int_)
            for obj, att in relations
            for ext, int_ in ((obj, att), (att, obj))
        )
    else:  # keep given order
        relations = frozenset(
            with_prefix(obj, att)
            for obj, att in relations
        )
    objects = tuple(frozenset(obj for obj, _ in relations))
    attributes = tuple(frozenset(att for _, att in relations))

    return Context(objects, attributes, relations)



def write_as_slf(context:Context, fname:str):
    """Write in given file the given context in slf format"""
    with open(fname, 'w') as fd:
        fd.write('[Lattice]\n')
        fd.write('{}\n{}\n'.format(len(context.objects), len(context.attributes)))
        fd.write('[Objects]\n')
        fd.write('\n'.join(map(str, context.objects)) + '\n')
        fd.write('[Attributes]\n')
        fd.write('\n'.join(map(str, context.attributes)) + '\n')
        fd.write('[relation]\n')
        for obj in context.objects:
            fd.write(' '.join('1' if (obj, att) in context.relations else '0'
                              for att in context.attributes) + ' \n')



def write_as_cxt(context:Context, fname:str):
    """Write in given file the given context in cxt format"""
    with open(fname, 'w') as fd:
        fd.write('B\n\n')
        fd.write('{}\n{}\n\n'.format(len(context.objects), len(context.attributes)))
        fd.write('\n'.join(map(str, context.objects)) + '\n')
        fd.write('\n'.join(map(str, context.attributes)) + '\n')
        for obj in context.objects:
            fd.write(''.join('X' if (obj, att) in context.relations else '.'
                              for att in context.attributes) + '\n')


def on_file(fname:str, outname:str, write=write_as_cxt):
    """The most simplistic call you can imagine"""
    write(extract_context(fname, prefixed=True), outname)
