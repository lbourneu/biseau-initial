"""Various helpers"""


import tkinter as tk


def add_write_callback(widget, callback:callable):
    """This is tkinter related. Two ways to do the same thing.
    The new one is not available everywhere.
    """
    try:  # first try the new way
        widget.trace_add("write", callback)
    except AttributeError:  # then the deprecated one
        widget.trace("w", callback)


class SimpleCheckbox(tk.Frame):
    """A checkbox able to say efficiently if it is activated or not,
    using its is_activated property.

    """

    def __init__(self, parent, text:str, state:bool, on_state_change:callable=None):
        tk.Frame.__init__(self, parent)
        self.create_widgets(state, text)
        self.on_state_change = on_state_change

    def create_widgets(self, initial_value:bool, text:str):
        self.value = tk.IntVar(value=initial_value)
        self.w_check = tk.Checkbutton(self, var=self.value, text=text)
        add_write_callback(self.value, lambda *_: self.state_changed())
        self.w_check.pack()

    @property
    def is_activated(self) -> bool:
        return bool(self.value.get())

    def get(self) -> bool:
        return self.is_activated

    def state_changed(self):
        if self.on_state_change:
            self.on_state_change()
