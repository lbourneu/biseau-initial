import networkx as nx
# from networkx.drawing import nx_agraph
import pygraphviz
from concepts import Context


def lattice_from_context(context:Context):
    return context.lattice

def concepts_map_from_lattice(lattice) -> dict:
    """Get the {(concept extent, concept intent): concept uid}
    from given lattice

    """
    return {
        (frozenset(concept.extent), frozenset(concept.intent)): concept.index
        for concept in lattice  # the concepts.Lattice object is iterable over concepts
    }

def graph_from_lattice(lattice) -> nx.DiGraph:
    """Convert given lattice in a networkx graph."""
    dot = lattice.graphviz().source.splitlines(True)
    # remove the fist comments
    heading_comments = []
    while dot[0].startswith('//'):
        comment, *dot = dot
        heading_comments.append(comment)
    # print('{} heading comments'.format(len(heading_comments)))

    # get the graph
    return nx.drawing.nx_agraph.from_agraph(
        pygraphviz.AGraph(''.join(dot)),
        create_using=nx.DiGraph()
    )



def colorize_nodes_by_name(graph:nx.DiGraph, color:str, condition:callable=None,
                           *, color_field:str='fillcolor'):
    """Give given fillcolor to nodes matching the condition.

    graph -- the graph to apply the transformation to (in place)
    color -- the color to feed color_field with
    condition -- a node -> bool application, restricting the nodes to color

    See graphviz color scheme: http://graphviz.org/content/color-names

    """
    assert type(graph) is nx.DiGraph, type(graph)
    if condition:  # valid callable
        nodes_to_color = (node for node in graph.nodes if condition(node))
    else:  # give it to every node
        nodes_to_color = graph.nodes
    props = {
        color_field: color,
        'gradientangle': 0.6,
    }
    nx.set_node_attributes(graph, {node: props for node in nodes_to_color})
    nx.set_node_attributes(graph, ' ', 'label')
    return graph


def colorize_nodes(graph:nx.DiGraph, node_colors:dict) -> nx.DiGraph:
    """Give given fillcolor to nodes matching the condition.

    graph -- the graph to apply the transformation to (in place)
    node_colors -- mapping node -> colors

    See graphviz color scheme: http://graphviz.org/content/color-names

    """
    HANDLED_COLORS = {'red', 'green', 'blue'}
    # print(node_colors)
    assert isinstance(node_colors, dict), node_colors
    for colors in node_colors.values():
        assert all(color in HANDLED_COLORS for color in colors)
    assert type(graph) is nx.DiGraph, type(graph)
    def colors_to_color(colors:set) -> str:
        """Return one color that represents the given ones"""
        if len(colors) == 1: return next(iter(colors))
        if len(colors) == 2:
            if 'red' in colors:
                if 'blue' in colors: return 'magenta'
                else: return 'yellow'  # red and green
            else: return 'cyan'  # blue and green
        if len(colors) == 3: return 'white'
        raise ValueError('UNVALID COLORS: ' + ', '.join(colors))
    props = {
        node: {'color': colors_to_color(colors),
               'gradientangle': 0.6}
        for node, colors in node_colors.items()
    }
    nx.set_node_attributes(graph, props)
    # nx.set_node_attributes(graph, ' ', 'label')
    return graph



def visualize(graph:nx.DiGraph, outfile:str='out.png'):
    agraph = nx.drawing.nx_agraph.to_agraph(graph)
    agraph.draw(outfile, prog='dot')


def aoc_poset(graph:nx.DiGraph) -> {str: (str, str)}:
    """Return AOC poset {node: (objects, attributes)}, so only one node hold
    a particular object/attribute"""
    attrs = {
        edge[0]: graph.get_edge_data(*edge) for edge in graph.edges
        if edge[0] == edge[1]  # source == target
    }
    return {
        node: (data.get('headlabel', ''), data.get('taillabel', ''))
        for node, data in attrs.items()
    }


def objects_and_attributes(graph:nx.DiGraph) -> {str: (str, str)}:
    """Return {node: (objects, attributes)}"""
    raise NotImplementedError()



if __name__ == '__main__':
    context = Context.fromstring('''
         |male|female|adult|child|
    man  |  X |      |  X  |     |
    woman|    |   X  |  X  |     |
    boy  |  X |      |     |  X  |
    girl |    |   X  |     |  X  |
    ''')
    graph = graph_from_lattice(context.lattice)
    aoc = aoc_poset(graph)
    print(aoc)
    assert 'woman' in aoc['c2'][0]
    def color_on_letter(aoc, letter:str) -> callable:
        return (lambda n: letter in ''.join(''.join(_) for _ in aoc.get(n, ())))
    import time
    import itertools
    letters = itertools.cycle(('acf'))

    while letters:
        colorize_nodes_by_name(graph, color='grey')
        colorize_nodes_by_name(graph, color='red', condition=color_on_letter(aoc, next(letters)))
        visualize(graph)
        time.sleep(1)
