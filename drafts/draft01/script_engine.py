


import os
import tkinter as tk
import glob
import importlib
import inspect
from collections import namedtuple

from utils import SimpleCheckbox


RETURNS_TYPES = {iter, str}
OPTIONS_TYPES = {int, float, bool, str}
SCRIPTS = frozenset(os.path.splitext(fname)[0]
                    for fname in map(os.path.basename, glob.glob('scripts/*.py'))
                    if not fname.startswith('_'))
Script = namedtuple('Script', 'name, module, options, return_mode, incompatible, active_by_default, inputs, outputs')
# name -- human readable name
# module -- reference to the module itself
# options -- list of (name, type) describing options
# return_mode -- iter or str, depending if the function is a generator or not
# incompatible -- list of incompatibles modules
# active_by_default -- true if the script must be activated at start
# inputs -- function in module to call to get the inputs knowing the parameters
# outputs -- function in module to call to get the outputs knowing the parameters


class ScriptWidget(tk.LabelFrame):
    """Widget for a Script"""

    def __init__(self, parent, script:Script):
        self.script = script
        self.chk_script = SimpleCheckbox(parent, text=self.script.name,
                                         state=self.script.active_by_default,
                                         on_state_change=self.update)
        super().__init__(parent, labelwidget=self.chk_script)
        self.create_widgets()

    def create_widgets(self):
        self.wid_options = {}
        for idx, (option_name, option_type) in enumerate(self.script.options, start=1):
            opt_name = option_name.replace('_', ' ')
            if option_type is int:
                wid = tk.Frame(self)
                lab = tk.Label(wid, text=opt_name + ':')
                lab.grid(row=1, column=1, sticky=tk.E)
                spn = tk.Spinbox(wid, from_=0, to=1000, increment=1)
                spn.grid(row=1, column=2, sticky=tk.W)
                wid.get = lambda: spn.get()
            elif option_type is float:
                wid = tk.Spinbox(self, text=opt_name)
            elif option_type is bool:
                wid = SimpleCheckbox(self, text=opt_name, state=False,
                                     on_state_change=self.update)
            else:
                raise ValueError("Unexpected option type {}.".format(option_type))
            wid.grid(row=1, column=idx, pady=5, padx=5)
            self.wid_options[option_name, option_type] = wid

    def update(self):
        self.master.update()

    @property
    def is_activated(self) -> bool:
        return self.chk_script.is_activated

    @property
    def options(self):
        return {
            name: wid.get()
            for (name, type), wid in self.wid_options.items()
        }

    def as_source(self, context, available_rules, actived_rules):
        if self.script.return_mode is str:
            ret = self.script.module.run(context, available_rules, actived_rules,
                                          **self.options)
        elif self.script.return_mode is iter:
            print()
            print('ITER!')
            ret = '\n'.join(self.script.module.run(
                context, available_rules, actived_rules, **self.options
            ))
        else:
            raise ValueError("Unexpected return mode {}".format(return_mode))
        print('RET:\n' + ret)
        print()
        return ret

    @property
    def isrulewidget(self) -> bool: return False

    @property
    def outputs(self):
        return self.script.outputs(**self.options)



def bad_script_error(script, msg:str):
    """Helper to raise errors while building a script"""
    raise ValueError("Module {} is not a valid script. {}."
                     ''.format(script.__file__, msg))


def build_all_scripts() -> iter:
    for script in SCRIPTS:
        yield build_script(script)


def build_script(module_name) -> Script:
    script = importlib.import_module('scripts.' + module_name)
    if not hasattr(script, 'NAME'):
        bad_script_error(script, "Attribute 'NAME' is missing")
    if not (hasattr(script, 'inputs') or hasattr(script, 'INPUTS')):
        bad_script_error(script, "Attribute 'INPUTS' is missing")
    if not (hasattr(script, 'outputs') or hasattr(script, 'OUTPUTS')):
        bad_script_error(script, "Attribute 'OUTPUTS' is missing")
    if not hasattr(script, 'run'):
        bad_script_error(script, "Function 'run' is missing")
    args = inspect.getfullargspec(script.run)
    print(args)

    # Return
    return_type = args.annotations['return']
    if return_type not in RETURNS_TYPES:
        bad_script_error(script, "Return type {} is not valid. Only {} are accepted"
                         "".format(return_type, ', '.join(map(str, RETURNS_TYPES))))

    # detect options
    print(args.kwonlyargs)
    options = []  # list of (arg name, arg type)
    for arg in args.kwonlyargs:
        argtype = args.annotations.get(arg)
        if argtype not in OPTIONS_TYPES:
            bad_script_error(script, "Option {} do not have a valid annotation "
                             "({}). Only {} are accepted"
                             "".format(arg, argtype, ', '.join(OPTIONS_TYPES)))
        options.append((arg, argtype))

    incompatible = frozenset(getattr(script, 'INCOMPATIBLE', ()))
    active_by_default = bool(getattr(script, 'ACTIVE_BY_DEFAULT', False))
    if hasattr(script, 'INPUTS') and not hasattr(script, 'inputs'):
        script.inputs = lambda *_: script.INPUTS
    if hasattr(script, 'OUTPUTS') and not hasattr(script, 'outputs'):
        script.outputs = lambda *_: script.OUTPUTS
    return Script(script.NAME, script, tuple(options),
                  return_type, incompatible, active_by_default,
                  script.inputs, script.outputs)



if __name__ == "__main__":
    print(tuple(build_all_scripts()))


