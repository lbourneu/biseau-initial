"""This prove that an image can be shown and updated easily using tkinter.

Greater objective : make it interactive (run code when mouse is over a node,
for instance).

"""

import tkinter as tk
from tkinter import font
import PIL
from PIL import ImageTk


DEFAULT_WM_TITLE = 'Gui testing: show an image'
NO_COLOR = 'white'
COLOR_BG_TEXT = 'light grey'
COLOR_UNSET = 'pink'
COLOR_ERR = 'red'
COLOR_LOG = 'dark green'
COLOR_OK = 'light green'
COLOR_WAITING = '#fd6b14'


class Application(tk.Frame):
    """Allow user to interact with an image.

    """

    def __init__(self, path:str, master=None):
        super().__init__(master or tk.Tk())
        self.master.wm_title(DEFAULT_WM_TITLE)
        self.path = path
        self.data = PIL.ImageTk.PhotoImage(PIL.Image.open(self.path))
        self.create_widgets()

    def create_widgets(self):
        self.picture = tk.Label(self, image=self.data)
        self.picture.pack()
        self.update = tk.Button(self, text='Refresh')
        self.update.pack()
        self.update.bind('<Button-1>', lambda _: self.update_image())
        self.pack()

    def update_image(self):
        self.data = PIL.ImageTk.PhotoImage(PIL.Image.open(self.path))
        self.picture.image = self.data
        self.picture.configure(image = self.data)


if __name__ == '__main__':
    win = Application('out.png')
    win.mainloop()
