"""Widget showing to user an high level visual of the data.

"""

import tkinter as tk

import PIL
from PIL import ImageTk


DEFAULT_VIZU_PATH = 'data/out.png'
DEFAULT_IMAGE_HEIGHT = 600
MAX_IMAGE_WIDTH = 800


class VisualizationWidget(tk.LabelFrame):
    """Widget allowing user to see the result of its actions.

    See doc at http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/labelframe.html

    """
    def __init__(self, vizu_path:str=DEFAULT_VIZU_PATH, master=None,
                 vizu_height:int=DEFAULT_IMAGE_HEIGHT,
                 name:str='Visualization', **kwargs):
        super().__init__(master=master, text=name, **kwargs)
        self.parent = master
        self.vizu_path = str(vizu_path)
        self.vizu_height = vizu_height
        self.retrieve_visualization()
        self.create_widgets()
        self.update_visualization()


    def create_widgets(self):
        # delete existing ones
        # for child in self.winfo_children():
            # child.destroy()
        self.lab_visualization = tk.Label(self, image=self.data, height=800)
        self.lab_visualization.pack()

        self.but_update = tk.Button(self, text='Refresh')
        self.but_update.bind('<Button-1>', lambda _: self.update_visualization())
        self.but_update.pack()


    def update_visualization(self):
        self.retrieve_visualization()
        self.show_visualization()

    def show_visualization(self):
        """Set the image in path as value of the visualization label"""
        self.lab_visualization.image = self.data
        self.lab_visualization.configure(image=self.data)

    def retrieve_visualization(self):
        """Set the image in path as value of the visualization label"""
        try:
            image = PIL.Image.open(self.vizu_path)
        except FileNotFoundError:
            self.data = None
            return
        size_ratio = self.vizu_height / image.height
        new_width = int(size_ratio * image.width)
        if new_width > MAX_IMAGE_WIDTH:
            size_ratio = MAX_IMAGE_WIDTH / new_width
            new_height = int(size_ratio * self.vizu_height)
            new_width = int(size_ratio * new_width)
        else:
            new_height = self.vizu_height
        image = image.resize((new_width, new_height), PIL.Image.ANTIALIAS)
        self.data = PIL.ImageTk.PhotoImage(image)
