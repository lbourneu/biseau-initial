"""Widget allowing user to define the ASP program.

"""

import tkinter as tk
from tkinter import font

from rule import Rule, RuleWidget
from utils import SimpleCheckbox
from script_engine import build_all_scripts, ScriptWidget


BLOCK_INPUT = '% INPUTS:'
RULES_FILE = 'data/rules.json'
EDITOR_WELCOME_MESSAGE = """% user can type ASP here to create its own rules.

color(red):- att("adult").
"""


class ProgramWidget(tk.LabelFrame):
    """Widget allowing user to define the ASP program.

    See doc at http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/labelframe.html

    """

    def __init__(self, rules_file:str, master=None, name:str='ASP Program',
                 rule_per_line:int=4, **kwargs):
        """

        rules_file -- json file where the rules are encoded
        rule_per_line -- how many rules to print by line

        """
        super().__init__(master=master, text=name, **kwargs)
        self.updatable = master
        self.rules_file = str(rules_file)
        self.rule_per_line = int(rule_per_line)
        self.update_rules()
        self.propapate_program_choice_in_user_input()


    def create_widgets(self):
        # delete existing ones
        for child in self.winfo_children():
            child.destroy()

        # Available rules
        self.wid_rules = []
        rules = (rule for name, rule in sorted(self.rules.items()))
        for idx, rule in enumerate(rules):
            if type(rule) is Rule:
                wid = RuleWidget(self, rule)
            else:  # it's a script
                wid = ScriptWidget(self, rule)
            row, col = idx // self.rule_per_line + 1, idx % self.rule_per_line + 1
            wid.grid(row=row, column=col)
            self.wid_rules.append(wid)
        next_row = row + 1

        # Text where user can write its own rules.
        self.wid_user_rule = tk.Text(self)
        self.wid_user_rule.bind('<KeyRelease>', lambda _: self.letter_written_by_user())
        self.wid_user_rule.insert(tk.END, EDITOR_WELCOME_MESSAGE)
        self.number_of_dot, self.number_of_percent = EDITOR_WELCOME_MESSAGE.count('.'), EDITOR_WELCOME_MESSAGE.count('%')
        self.wid_user_rule.grid(row=next_row, columnspan=self.rule_per_line)

        # (auto-)run buttons
        self.grp_run = tk.Frame(self)

        self.txt_models_count = tk.StringVar('')
        self.lab_models_count = tk.Label(self.grp_run, textvariable=self.txt_models_count)
        self.lab_models_count.grid(row=1, column=1, padx=15, sticky=tk.W)

        self.but_run = tk.Button(self.grp_run, text='Run')
        self.but_run.bind('<Button-1>', lambda _: self.update())
        self.but_run.grid(row=1, column=2, padx=15, pady=10, sticky=tk.E)

        self.but_autorun = SimpleCheckbox(self.grp_run, text='auto-run', state=True)
        self.but_autorun.grid(row=1, column=3, padx=15, pady=10, sticky=tk.W)

        self.grp_run.grid(row=next_row+1, column=1, columnspan=self.rule_per_line)


    def propapate_program_choice_in_user_input(self):
        """Modify the user input text based on selected rules"""
        # detect already written lines (begin with '% INPUTS')
        # block_start and block_end are the first occurence of INPUT block
        user_lines = self.user_rule.splitlines(True)
        lines = iter(user_lines)
        line_counter = -1  # indexing starts at 0, but incr is done before treatments
        block_start, block_end = None, None
        for line in lines:
            line_counter += 1
            if line.strip() == BLOCK_INPUT:
                # print('INPUT BLOCK DETECTED AT', line_counter)
                block_start = line_counter
                block_end = block_start
                for line in lines:
                    line_counter += 1
                    if line.startswith('%   -'):
                        block_end = line_counter
                    else:
                        # print('BLOCK END DETECTED AT', line_counter)
                        break
            elif block_start and block_end:  # already found
                break
            else:  # not already found
                pass

        # compute the line to show
        if any(self.actived_rules):
            to_include = BLOCK_INPUT + '\n' + ''.join(
                '%   - ' + output + '\n'
                for rule in self.actived_rules
                for output in rule.outputs
            )
        else:  # no rule activated == no output == no INPUT block
            to_include = ''

        # print('START END INDEXES:', block_start, block_end)
        # Modify the Text widget accordingly
        if block_start is not None:  # a block as been found
            user_lines[block_start:block_end+1] = to_include
            new_user_lines = ''.join(user_lines)
            self.wid_user_rule.delete(1.0, tk.END)  # delete all text
            self.wid_user_rule.insert(1.0, new_user_lines)
        else:  # no block: let's just add it to the beginning
            self.wid_user_rule.insert(1.0, to_include)
        # print()


    def update(self):
        """Called by children when source code is modified"""
        self.propapate_program_choice_in_user_input()
        self.updatable.update(asp_code=True)


    def update_rules(self):
        self.retrieve_rules()
        self.create_widgets()


    def retrieve_rules(self):
        """Retrieve rules from memory"""
        self.rules = {rule.name: rule for rule in Rule.from_file(RULES_FILE)}
        self.rules.update({script.name: script for script in build_all_scripts()})



    @property
    def user_rule(self):
        """Return the source code written by user"""
        return self.wid_user_rule.get(1.0, tk.END)

    @property
    def actived_rules(self):
        yield from (
            widrule.rule if widrule.isrulewidget else widrule
            for widrule in self.wid_rules
            if widrule.is_activated
        )

    def generated_sources(self, payload) -> str:
        return '\n'.join(rule.as_source(*payload) for rule in self.actived_rules) + '\n' + self.user_rule


    def letter_written_by_user(self):
        """If there is a different number of dot or % than before,
        a compilation must be ran.

        TODO: this is enough for poc, but a better detection of interesting
        changes could be done, by monitoring what the insertion really do
        (commenting a populated line would be a reason to re-run the computations.
        Commenting an empty line is not. Adding a dot in comments is not.)
        It could be not that difficult to just parse the comment space,
        and count characters out of comments. A change due to '.' or '%'
        insertion/deletion would be a valid reason to re-run the computations.

        """
        current_dots = self.user_rule.count('.')
        current_percents = self.user_rule.count('%')
        if self.number_of_dot != current_dots or self.number_of_percent != current_percents:
            if self.but_autorun.is_activated:
                self.update()
        self.number_of_dot = current_dots
        self.number_of_percent = current_percents


    def set_models_count(self, number:int):
        """setter for the number of models generated"""
        if number is None:
            self.txt_models_count.set('No model generated')
        else:
            self.txt_models_count.set(str(number) + ' models generated')
