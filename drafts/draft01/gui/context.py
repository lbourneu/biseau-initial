"""Widget helping user picking a context to use.

"""

import os
import glob
import tempfile
import tkinter as tk
from tkinter import ttk

from concepts import Context
from utils import graph_asp_to_standard


CONTEXTS_GLOB = 'contexts/*.[ctxsvlp]*'


class ContextWidget(ttk.Frame):
    def __init__(self, master, contexts_glob:str=CONTEXTS_GLOB):
        super().__init__(master)
        self.updatable = master
        self.contexts_glob = str(contexts_glob)
        self.current_context = None
        self.create_widgets()

    @property
    def available_contexts(self):
        return tuple(glob.iglob(self.contexts_glob))

    def newselection(self, event):
        self.current_context = ContextWidget._compute_context(self.box.get())
        self.current_context.source = self.current_context_source
        if self.updatable:
            self.updatable.update(context=True)
        else:
            print('No parent, so here is the new context:', self.current_context)

    def create_widgets(self):
        self.box_value = tk.StringVar(self, 'Choose a context')
        self.box = ttk.Combobox(self, textvariable=self.box_value,
                                justify='left', state='readonly',
                                values=self.available_contexts)
        self.box.bind("<<ComboboxSelected>>", self.newselection)
        self.box.pack()
        self.pack()

    @property
    def current_context_source(self) -> str:
        """Return the ASP source code describing the context"""
        context = self.current_context
        # print(dir(context))
        # print(context.objects)
        # print(context.properties)
        # print(context.bools)
        assert len(context.objects) == len(context.bools)
        assert len(context.properties) == len(context.bools[0])
        return '%% Context encoding\n' + '\n'.join(
            'rel("{}","{}").'.format(obj, att)
            for obj, line in zip(context.objects, context.bools)
            for att, hold in zip(context.properties, line)
            if hold
        )

    @staticmethod
    def _compute_context(fname:str) -> Context:
        """Return the Context object describing the context in given file"""
        # See https://concepts.readthedocs.io/en/stable/manual.html#persistence
        #  for mapping extension->format needed by concepts module.
        format_map = {'txt': 'table'}
        extension = os.path.splitext(fname)[1][1:]
        if extension == 'lp':  # ASP encoded graph: a primary step is required
            with tempfile.NamedTemporaryFile('w', suffix='.cxt', delete=False) as fd:
                print('Input is ASP-encoded. Conversion in {}…'.format(fd.name),
                      end='', flush=True)
                graph_asp_to_standard.on_file(fname, fd.name)
                fname = fd.name
                extension = 'cxt'
                print(' done')
        format = format_map.get(extension, extension)  # some use the same name
        return Context.fromfile(fname, frmat=format)

    def select(self, context_name:str):
        context = 'contexts/{}'.format(context_name)
        assert context in self.available_contexts
        self.box.set(context)
        self.newselection(None)



if __name__ == '__main__':
    win = ContextChoice(None)
    win.mainloop()
