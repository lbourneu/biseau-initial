"""Biseau script.

Once activated, will aggregate Concepts
and AOC poset as a single model.

"""

import clyngor


NAME = 'concepts as a single model'
INCOMPATIBLE = {'Concepts', 'AOC poset'}  # they generate weird things.

def inputs(aoc_poset:bool=False, only_aoc:bool=False, start_index:int=0):
    return {'rel/2'}

def outputs(aoc_poset:bool=False, only_aoc:bool=False, start_index:int=0):
    keys = ('obj', 'att') if not only_aoc else ()
    keys += ('specobj', 'specatt') if aoc_poset else ()
    return frozenset(keys)

def run(context, available_rules:dict, actived_rules:dict, *,
        aoc_poset:bool=False, only_aoc:bool=False, start_index:int=0) -> iter:
    concept_rule = available_rules['Concepts']
    aocposet_rule = available_rules['AOC poset']

    asp_code = context.source + concept_rule.as_source() + (aocposet_rule.as_source() if aoc_poset else '')

    models = clyngor.solve([], inline=asp_code).by_predicate
    keys = outputs(aoc_poset, only_aoc, start_index)
    for idx, model in enumerate(models, start=int(start_index)):
        for key in keys:
            thgs = model.get(key, ())
            for thg in thgs:
                thg, = thg  # first arg only: obj and att have an arity of 1
                yield '{}({},{}).'.format(key, idx, thg)




