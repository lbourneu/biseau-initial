
import os
import itertools
from pprint import pprint
import clyngor
import pygraphviz
import networkx as nx


FILE_CONTEXT = 'contexts/boolean_3var.lp'
ASP_CONCEPTS = 'asp/yield_concepts.lp'
ASP_LATTICES = 'asp/build_lattice.lp'
ASP_SUPINFIMUMS = 'asp/build_inum.lp'
OUT_DOT = 'out/lattice.dot'
OUT_PNG = 'out/lattice.png'
CONCEPT_INDEX_START = 1


def compute_context(context_aspfile:str=FILE_CONTEXT) -> str:
    """Return file containing asp rules describing the concept as rel/2"""
    extension = os.path.splitext(context_aspfile)[1][1:]
    if extension == 'lp':
        return context_aspfile
    else:
        raise NotImplementedError()


def compute_concepts(context_aspfile:str) -> iter:
    """Yield atoms describing the concepts in given context."""
    models = tuple(itertools.chain(
        clyngor.solve([context_aspfile, ASP_CONCEPTS]).by_predicate,
        clyngor.solve([context_aspfile, ASP_SUPINFIMUMS]).by_predicate,
    ))
    keys = {'ext', 'int'}
    for idx, model in enumerate(models, start=CONCEPT_INDEX_START):
        for key in keys:
            for thg in model.get(key, ()):
                assert len(thg) == 1, "obj and att must have an arity of 1"
                yield '{}({},{}).'.format(key, idx, thg[0])

# compute the lattice
def graph_from_relations(relations:iter) -> iter:
    """Yield dot lines representing the graph given as an iterable
    of (source, target).

    relations -- iterable of (source uid, target uid)

    """
    yield 'Digraph Lattice {\n'
    yield '\tnode [label="" shape=circle style=filled width=.25]\n'
    yield '\tedge [dir=none labeldistance=1.5 minlen=2]\n'
    treated_nodes = set()  # contains nodes already treated
    for source, target in relations:
        for node in source, target:
            if node not in treated_nodes:
                treated_nodes.add(node)
                yield '{n} -> {n} [color=transparent labelangle=270]'.format(n=node)
        yield '\t{}->{};\n'.format(source, target)
    yield '}'

def compute_concepts_rules(concepts:iter) -> iter:
    """Yield (bigger, smaller) relations about concepts found in input iterable."""
    asp = ''.join(concepts)
    # print('ASP:', asp)
    models = clyngor.solve([ASP_LATTICES], inline=asp).by_predicate
    for model in models:
        yield from model.get('under')

if __name__ == "__main__":
    print(''.join(graph_from_relations(
        compute_concepts_rules(compute_concepts(compute_context()))
    )))
